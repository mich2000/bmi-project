# Project BMI management system

## Beschrijving

Het idee is om een systeem te maken waarin de gebruiker zijn gewicht en lengte kan volgen per datum. Dit geeft de mogelijkheid aan de gebruiker om zijn gewicht/lengte evolutie te volgen. Hierbij zal het gewicht een belangrijkere rol nemen, want de lengte van de gebruiker verandert trager en zal op een bepaalde moment stoppen met veranderen. De gebruiker/patieënt zou 1 keer per week zijn gewicht en lengte moeten toevoegen.

De gegevens die de gebruiker invoert zullen geanalyseerd worden door zijn dokter of fitness coach. Op basis van de het gewicht en de lengte zal men de BMI, BSA en het ideale gewicht voor de persoon kunnen berekenen.

* Uitleg over de termen **BMI, BSA en het ideal gewicht** worden later uitgelegd in het deel **[Uitleg medische termen](#uitleg-medische-meetfactoren)**

## Rollen en applicatie layout

1. **Gewone gebruiker**(user): Deze rol zal is voor de gewone mensen die hun eigen gewicht en lengte gaat volgen. Hij zal alleen CRUD operaties kunnen uitvoeren op zijn eigen account en gezondheidsgegevens. De gebruiker zal de mogelijkheid hebben om zijn gegevens te exporteren als een CSV file.
2. **Coach of dokter**(Coach): Deze zal met toestemming van de gebruiker, toegang kunnen hebben tot zijn gegevens en zijn evolutie kunnen volgen. Op de personen waarvan hij de toestemmingen heeft kan hij hun documenten ook exporteren als CSV file.
3. **Administrator**(Admin): Deze rol zal bevoegdheid kunnen hebben over het hele systeem en zal toegang hebben tot alle gegevens van alle gebruikers.

## Doelgroep

Mijn applicatie is bedoeld voor mensen die hun gewicht en lengte willen volgen voor fitness of gezondheidsredenen. Voor de mensen die het voor gezondheidsredenen(diabeet, zwaarlijvigheid) de applicatie gebruiken, is het aangeraden om zich te laten volgen door een dokter of diëetist en toestemming aan deze geven om de gegevens te bekijken.

## Gebruikte technologiëen en fases

1. In de 1ste fase ga ik aan mijn basisklasses werken en deze uit testen.
    * Technologiëen: C# + C# Test Framework
    * Geschatte tijd: 3 - 5 uur
2. Ik ga aan versimpelde versie van mijn app werken, waarin maar 1 rol in zit voor de gebruiker. Ik ga ook als database SqlLite gebruiken omdat deze simpeler is om te configureren. Deze fase zal ook nuttig zijn om grafieken al te testen, deze zullen gemaakt zijn door Javascript met de library Chart.js of Chartist.js. De grafieken wil uit testen voor het gewicht en BMI. Het is ook een goede opportuniteit om mijn exporten al te testen. Ik zal voor SqlLite geen migraties doen, maar deze configureren dat bij het starten van de app, het de database verwijdert en weer aanmaakt.
    * Technologiëen: C# + ASP.net Core + SqlLite + Javascript(jQuery, jQuery Validate, Chart.js, Chartist.js) + CSS(Bootstrap)
    * Geschatte tijd: 10 - 15 uur
3. In deze fase ga ik mijn complete applicatie maken, met alle verwachte rollen. Ook als database deze keer Sql Server van Microsoft gebruiken. Ik ga zoveel mogelijk code overpakken van het prototype fase. Ik ga ook Logging implementeren in deze fase.
    * Technolgiëen: C# + ASP.net Core + Microsoft Sql Server + Javascript(jQuery, jQuery Validate, Chartist.js) + CSS(Bootstrap)
    * Geschatte tijd: 30 - 40 uur

* Notitie:
    * Voor de fases 2 en 3 ben ik niet helemaal zeker dat ik dit juist geschat hebt, omdat ik geen ervaring heb met chart.js(lijkt vrij gemakkelijk om te beginnen) en van rollen binnen asp.net
    * Ik heb voor de grafieken javascript framework Chartist.js gebruitk omdat ik deze gemakkelijker vond in gebruik en zal deze ook gebruiken voor mijn project.

## Opzetten van de Bmi server

### Voorbereiding

In mijn programma heb ik deze zo geontwerpt dat bij de start van het programma een migratie uitvoert om een database aan te maken als deze al dan niet aangemaakt is. De database die gebruikt wordt is Sql server van microsoft en de database connectie die gebruikt zal worden is `server=(localdb)\\MSSQLLocalDB;database=BmiDB;Trusted_Connection=true;MultipleActiveResultSets=true;`. Het is belangrijk dat sql server beschikbaar is voor de webserver anders zal deze niet kunnen werken. Als u een andere database wil aan geven moet u gewoon de database attribuut in de connectie string van naam veranderen.

De webserver zal ook aan logging doen d.m.v. nlog logging framework. De map waarin de logs bijgehouden is in de map van documenten van de gebruiker, logs is dan in de BMI-Server map waarin de logs uiteindelijk zijn. U kunt de locatie van log files veranderen in de nlog.config file.

Er worden ook een paar mock gebruikers en coaches aangemaakt als het programma op gestart wordt, als u dit niet wil dan moet u de startup file van het asp.net core project de laatste lijn van de Configure methode wegdoen. Een superadmin wordt ook aangmeaakt.

![ code lijn verantwoordelijk voor het mokken van de personen en coaches in de bmi server ](fotos/seedUsersAndCoaches.png)

* Mock gebruikers in rol User:
    * email: "ps1@bmidomain.com", wachtwoord: "Person"
    * email: "ps2@bmidomain.com", wachtwoord: "Person"
    * email: "ps3@bmidomain.com", wachtwoord: "Person"

* Mock gebruikers in rol Coach:
    * email: "coach1@bmidomain.com", wachtwoord: "Coach"
    * email: "coach2@bmidomain.com", wachtwoord: "Coach"

* Admin:
    * email: "admin@bmidomain.com", wachtwoord: "AdminPass"

Een keer dat alles naar u wil is geconfigureert dan moet u deze compileren en een release versie produceren. Ik heb hiervoor een speciale release profiel gemaakt dat op windows 10 werkt, deze heet **ProjectBmiServer**. Deze release profiel geeft u een getrimmde vrijstaande binary met weinig files.

### Uitvoering

* Stappenplan om bmi server op te starten:
    1. Command line opendoen.
    2. Naar de map waarin de bmi server executable zit navigeren.
    3. Voer de executable `Bmi.Production.WebApp.exe`. ![command line die bmi server gaat op starten](fotos/execute-BmiServer.png)

![Bmi server die op de command line zijn output zet](fotos/executed-BmiServer.png)

**(optioneel): Verander zo vroeg mogelijk het wachtwoord van de Admin gebruiker.**

In het begin van de uitvoering van de bmi server, wanneer er nog geen database voor is kan het een tijdje duren voordat deze operationeel is. Maar een keer dat er een database is zal deze sneller operationeel zijn.

## Uitleg medische meetfactoren

### BMI(Body Mass Index)

BMI is een manier om een persoon te categoriseren op basis van zijn gewicht en van zijn lengte. De basis categoriëen zijn ondergewicht, normale gewicht, overgewicht en obesitas. BMI wordt dus gebruikt om indicatie te geven van de gezondheid van de patiënt.

* Formule BMI: `BMI cijfer = gewicht(in kilogram)/lengte(in meter)²`

* Categorieën BMI volgens Belgische normen:
    * Ondergewicht: kleiner dan 18.5
    * Normale gewicht: Tussen 18.5 en 25
    * Overgewicht: Tussen 25 en 30
    * Obesitas: Groter dan 30

### BSA(Body Surface Area)

BSA is een manier om het oppervlakte van een mens te meten. Het heeft een aantal toepassingen in het medische veld, maar in BMI blijft nog steeds praktischer. Om BSA te berekenen hebben wij veel formules, maar de acuraatste is de Du Bois formule.

* Formule Du Bois: `BSA(resultaat in m²) = 0.007184 * (gewicht(in kg)^0.425) * (lengte(in cm)^0.725)`

### Ideale gewicht

Om de ideal gewicht voor een persoon te berekenen, moeten we eerst bepalen wat een goede BMI zou zijn. Ik heb als BMI 21.75 gekozen omdat deze in het midden is tussen 18.5 en 25. Om de formule te hebben om op basis van bmi en lengte het gewicht te berekenen, moeten we de bmi formule omvormen.

* Formule voor gewicht op basis van bmi en lengte: `gewicht = bmi * (lengte * lengte)`

## Referenties
* Link Chart.js: https://www.chartjs.org/
* Link naar Chartist.js: http://gionkunz.github.io/chartist-js/
* Link gedetailleerde uitleg BMI: https://en.wikipedia.org/wiki/Body_mass_index
* Link BMI volgens belgische normen: https://www.gezondleven.be/themas/voeding/obesitas-en-overgewicht/body-mass-index-bmi
* Link uitleg BSA: https://en.wikipedia.org/wiki/Body_surface_area
* link voor SqlLite & EF Core samen: http://bekenty.com/use-sqlite-in-net-core-3-with-entity-framework-core/