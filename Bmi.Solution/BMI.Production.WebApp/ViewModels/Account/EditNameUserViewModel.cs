﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace BMI.Production.WebApp.ViewModels.Profile
{
    public class EditNameUserViewModel
    {
        #region contstructor
        public EditNameUserViewModel(string userName)
        {
            UserName = userName;
        }

        public EditNameUserViewModel() { }
        #endregion

        #region properties
        [PersonalData]
        [Required]
        [Display(Name = "User Name: ")]
        public string UserName { get; set; }
        #endregion
    }
}