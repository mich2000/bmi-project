﻿using BMI.Production.WebApp.Enums;
using System.ComponentModel.DataAnnotations;

namespace BMI.Production.WebApp.ViewModels.Account
{
    public class RegisterViewModel
    {
        #region constructors
        public RegisterViewModel(string email, string password, string confirmPassword, ChosableRoles chosenRoles)
        {
            Email = email;
            Password = password;
            ConfirmPassword = confirmPassword;
            ChosenRoles = chosenRoles;
        }

        public RegisterViewModel(string email, string password, string confirmPassword)
            : this(email, password, confirmPassword, ChosableRoles.User) { }

        public RegisterViewModel() : this("", "", "") { }
        #endregion

        #region properties
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Comfirm password")]
        [Compare("Password", ErrorMessage = "Password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [Display(Name = "Choose the account role")]
        public ChosableRoles ChosenRoles { get; set; }
        #endregion
    }
}