﻿using BMI.Production.DAL.Models;
using System.Collections.Generic;

namespace BMI.Production.WebApp.ViewModels.Coach
{
    public class UserViewViewModel
    {
        public UserViewViewModel(IEnumerable<BmiLog> bmiLogs, string userId)
        {
            BmiLogs = bmiLogs;
            UserId = userId;
        }

        public IEnumerable<BmiLog> BmiLogs { get; set; }

        public string UserId { get; set; }
    }
}