﻿using BMI.Production.DAL.Models;
using System.Collections.Generic;
using System.Linq;

namespace BMI.Production.WebApp.ViewModels.Coach
{
    public class RequestCoachViewModel
    {
        #region constructors
        public RequestCoachViewModel(string user, IEnumerable<AuthorizedID> acceptedRequests, IEnumerable<AuthorizedID> failedPendingRequest)
        {
            UserId = user;
            AcceptedRequests = acceptedRequests;
            FailedPendingRequest = failedPendingRequest;
        }
        #endregion

        #region properties
        public string UserId { get; set; }
        public IEnumerable<AuthorizedID> AcceptedRequests { get; set; }
        public IEnumerable<AuthorizedID> FailedPendingRequest { get; set; }
        #endregion

        #region methods
        public int CountAllRequest()
        {
            return AcceptedRequests.Count() + FailedPendingRequest.Count();
        }
        #endregion
    }
}
