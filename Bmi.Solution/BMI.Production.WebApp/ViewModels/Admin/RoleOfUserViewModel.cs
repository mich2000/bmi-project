﻿namespace BMI.Production.WebApp.ViewModels.Admin
{
    public class RoleOfUserViewModel
    {
        public string Name { get; set; }

        public bool UserInRole { get; set; }
    }
}