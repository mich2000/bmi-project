﻿using System.ComponentModel.DataAnnotations;

namespace BMI.Production.WebApp.ViewModels.Admin
{
    public class AuthRequestsForAdminViewModel
    {
        [EmailAddress]
        [Required]
        [Display(Name = "Email of the user that sends the request")]
        public string EmailOfTheSender { get; set; }

        [EmailAddress]
        [Required]
        [Display(Name = "Email of the requested user")]
        public string EmailOfRequested { get; set; }
    }
}