﻿//config of the weight graph, I got it off the get started page of chartist.js.
var options = {
    showPoint: true,
    height: 300,
    axisX: {
        showGrid: true,
        showLabel: true
    },
    axisY: {
        offset: 60,
        showGrid: true,
        showLabel: true,
        labelInterpolationFnc: (value) => value,
    },
    chartPadding: 0,
    low: 0
};
new Chartist.Line(document.querySelector('#chartBMI'), {
    name: 'BMI',
    series: [JSON.parse(chartBMI.dataset.weight)]
}, options);