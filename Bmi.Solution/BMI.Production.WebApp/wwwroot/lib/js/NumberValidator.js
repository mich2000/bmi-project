﻿//link used to validate decimale with dot and comma seperator: http://blog.rebuildall.net/2011/03/02/jQuery_validate_and_the_comma_decimal_separator
//if script used in production there should be minified version of it
$.validator.methods.range = function (value, element, param) {
    var globalizedValue = value.replace(",", ".");
    return this.optional(element) || (globalizedValue >= param[0] && globalizedValue <= param[1]);
}

$.validator.methods.number = function (value, element) {
    return this.optional(element) || /^-?(?:\d+|\d{1, 3}(?:[\s\.,]\d{3})+)(?:[\.,]\d+)?$/.test(value);
}