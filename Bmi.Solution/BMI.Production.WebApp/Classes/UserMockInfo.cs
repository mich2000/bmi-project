﻿namespace BMI.Production.WebApp.Classes
{
    /**
     * Static class containing email and begin password of the admin role.
     * **/
    public static class UserMockInfo
    {
        /**
         * Admin
         * **/
        public static readonly (string email, string pwd) AdminInfo = ("admin@bmidomain.com", "AdminPass");

        /**
         * Person mock users
         * **/
        public static readonly (string email, string pwd) Person1 = ("ps1@bmidomain.com", "Person");
        public static readonly (string email, string pwd) Person2 = ("ps2@bmidomain.com", "Person");
        public static readonly (string email, string pwd) Person3 = ("ps3@bmidomain.com", "Person");

        /**
         * Coach mock users
         * **/
        public static readonly (string email, string pwd) Coach1 = ("coach1@bmidomain.com", "Coach");
        public static readonly (string email, string pwd) Coach2 = ("coach2@bmidomain.com", "Coach");
    }
}