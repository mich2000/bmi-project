﻿using BMI.Production.DAL.Classes;
using BMI.Production.DAL.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BMI.Production.WebApp.Controllers
{
    [AllowAnonymous]
    public class ErrorController : Controller
    {
        private readonly ILogger<ErrorController> _logger;

        public ErrorController(ILogger<ErrorController> logger)
        {
            _logger = logger;
        }

        [Route("Error/{statusCode}")]
        public IActionResult HttpStatusCodeHandler(int statusCode)
        {
            IStatusCodeReExecuteFeature statusCodeResult = HttpContext.Features.Get<IStatusCodeReExecuteFeature>();
            IUrl url = new FaulthyUrl(statusCode, statusCodeResult.OriginalPath, statusCodeResult.OriginalQueryString);
            _logger.LogError(url.StatusCodeMessage());
            return View("NotFound", url);
        }

        [Route("Error")]
        public IActionResult ExceptionHandler()
        {
            IExceptionHandlerPathFeature exceptionHandlerPathFeature = HttpContext.Features.Get<IExceptionHandlerPathFeature>();
            WebFailure failure = new WebFailure(exceptionHandlerPathFeature.Error);
            _logger.LogError(failure.LogError());
            return View("Error", failure);
        }
    }
}