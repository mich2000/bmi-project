﻿using BMI.Production.DAL.Classes;
using BMI.Production.DAL.Enums;
using BMI.Production.DAL.Interfaces;
using BMI.Production.DAL.Models;
using BMI.Production.WebApp.Classes;
using BMI.Production.WebApp.Interfaces;
using BMI.Production.WebApp.ViewModels.Admin;
using BMI.Production.WebApp.ViewModels.Coach;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BMI.Production.WebApp.Controllers
{
    [Authorize(Roles = Roles.Admin)]
    [Authorize(Policy = "MustBeSignedIn")]
    public class AdminController : Controller, IIdentifierController
    {
        #region fields
        private readonly ILogger<AdminController> _logger;
        private readonly UserManager<BmiUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IBmiRepo _repo;
        #endregion

        #region constructor
        public AdminController(ILogger<AdminController> logger, UserManager<BmiUser> userManager,
            RoleManager<IdentityRole> roleManager, IBmiRepo repo)
        {
            _logger = logger;
            _userManager = userManager;
            _roleManager = roleManager;
            _repo = repo;
        }
        #endregion

        #region User Info
        [Authorize(Policy = "MustBeSignedIn")]
        public string UserId() => User.FindFirstValue(ClaimTypes.NameIdentifier);

        [Authorize(Policy = "MustBeSignedIn")]
        public string UserEmail() => User.FindFirstValue(ClaimTypes.Email);

        [Authorize(Policy = "MustBeSignedIn")]
        public string UserRole() => User.FindFirstValue(ClaimTypes.Role);
        #endregion

        #region HttpGet
        [HttpGet]
        public IActionResult Index()
        {
            UsersViewModel allUsersViewModel = new UsersViewModel();
            foreach (BmiUser user in _userManager.Users)
            {
                if (_userManager.IsInRoleAsync(user, Roles.Coach).Result && _userManager.IsInRoleAsync(user, Roles.Person).Result)
                {
                    allUsersViewModel.AllUsersAndCoaches.Add(user);
                }
                else if (_userManager.IsInRoleAsync(user, Roles.Coach).Result)
                {
                    allUsersViewModel.AllCoaches.Add(user);
                }
                else if (_userManager.IsInRoleAsync(user, Roles.Person).Result)
                {
                    allUsersViewModel.AllUsers.Add(user);
                }
            }
            return View(allUsersViewModel);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return RedirectToAction("Register", "Account");
        }

        [HttpGet]
        public IActionResult CoachView(string userId)
        {
            IEnumerable<AuthorizedID> requests = _repo.GetAuthRequestFromCoach(userId);
            RequestCoachViewModel requestViewModel = new RequestCoachViewModel(userId,
                requests.Where(auth => auth.StatusRequest == Status.Accepted),
                requests.Where(auth => auth.StatusRequest != Status.Accepted));
            return View(requestViewModel);
        }

        [HttpGet]
        public IActionResult UserView(string userId)
        {
            IEnumerable<BmiLog> bmiLogs = _repo.GetBmiLogsByUser(userId);
            return View(new UserViewModel(userId, bmiLogs));
        }

        [HttpGet]
        public IActionResult CreateLog()
        {
            return View();
        }

        [HttpGet]
        public IActionResult UpdateLogOfUser(string userId, int id)
        {
            BmiLog log = _repo.GetBmiLog(userId, id) ?? throw new ArgumentNullException();
            if (log.ID == 0) throw new ArgumentException();
            return View(log);
        }

        [HttpGet]
        public IActionResult DeleteLogOfUser(string userId, int id)
        {
            BmiLog log = _repo.GetBmiLog(userId, id) ?? throw new ArgumentNullException();
            if (log.ID == 0)
                throw new ArgumentException();
            return View(log);
        }

        [HttpGet]
        public async Task<IActionResult> RemoveUser(string userId)
        {
            BmiUser gebruiker = await _userManager.FindByIdAsync(userId) ?? throw new ArgumentNullException();
            return View(gebruiker);
        }

        [HttpGet]
        public async Task<IActionResult> UpdateUser(string userId)
        {
            BmiUser gebruiker = await _userManager.FindByIdAsync(userId) ?? throw new ArgumentNullException();
            return View(new UpdateUserViewModel(gebruiker.Id, gebruiker.UserName));
        }

        [HttpGet]
        public IActionResult CreateAuth()
        {
            return View();
        }

        [HttpGet]
        public IActionResult UpdatePasswordUser(string userId)
        {
            if (!string.IsNullOrEmpty(userId))
            {
                return View(new EditPasswordOfUserViewModel() { UserId = userId });
            }
            return RedirectToAction("Index", "Admin");
        }

        [HttpGet]
        public async Task<IActionResult> UpdateRolesUser(string userId)
        {
            if (!string.IsNullOrEmpty(userId))
            {
                BmiUser user = await _userManager.FindByIdAsync(userId);
                if (user != null)
                {
                    UserRolesStatusViewModel statusRolesUser = new UserRolesStatusViewModel() { UserId = userId };
                    foreach (IdentityRole role in _roleManager.Roles.AsEnumerable())
                    {
                        bool userPossesRole = _userManager.IsInRoleAsync(user, role.Name).Result;
                        if (role.Name != Roles.Admin)
                        {
                            statusRolesUser.RolesStatus.Add(new RoleOfUserViewModel()
                            {
                                Name = role.Name,
                                UserInRole = userPossesRole
                            });
                        }
                    }
                    return View(statusRolesUser);
                }
            }
            return RedirectToAction("Index", "Admin");
        }

        [HttpGet]
        public IActionResult RemoveAuth(string userId, string coachId)
        {
            if (!string.IsNullOrEmpty(userId))
            {
                AuthorizedID auth = _repo.GetSpecificAuthRequest(coachId, userId);
                if (auth != null)
                {
                    return View(auth);
                }
            }
            return View();
        }

        [HttpGet]
        public IActionResult AuthPageOfUser(string userId)
        {
            if (!string.IsNullOrEmpty(userId))
            {
                IEnumerable<AuthorizedID> authorizedIDs = _repo.GetAuthRequestFromUser(userId);
                return View(new AuthRequestsOfUserViewModel()
                {
                    AuthorizedIDs = authorizedIDs,
                    UserId = userId
                });
            }
            return RedirectToAction("Index", "Admin");
        }

        [HttpGet]
        public IActionResult ModifyAuthStatus(string userId, string coachId)
        {
            AuthorizedID auth = _repo.GetSpecificAuthRequest(coachId, userId) ?? throw new ArgumentNullException();
            return View(auth);
        }
        #endregion

        #region HttpPost

        [HttpPost]
        public async Task<IActionResult> CreateLog(LogCreationViewModel model)
        {
            if (ModelState.IsValid)
            {
                BmiUser user = await _userManager.FindByEmailAsync(model.Email);
                if (user != null)
                {
                    if (_userManager.IsInRoleAsync(user, Roles.Person).Result)
                    {
                        BmiLog log = _repo.InsertLog(new BmiLog(model.DateLog, user.Id, model.Lenght, model.Weight));
                        if (log != null)
                        {
                            _repo.CreateLogHistoryInsert(log.ID, UserId());
                            return RedirectToAction("UserView", "Admin", new { userId = user.Id });
                        }
                        ModelState.AddModelError(string.Empty, "Log couldn't be made nor added");
                        _logger.LogWarning("Log couldn't be made nor added");
                    }
                }
                ModelState.AddModelError(string.Empty, "User can't be found or is not in the role of person.");
                _logger.LogWarning("User can't be found");
            }
            return View(model);
        }

        [HttpPost]
        public IActionResult DeleteLogOfUser(BmiLog bmiLog)
        {
            BmiLog bmiLogToBeDeleted = _repo.GetBmiLogsByUser(bmiLog.IdUser).FirstOrDefault(log => log.ID == bmiLog.ID);
            if (bmiLogToBeDeleted != null)
            {
                BmiLog resultDeletion = _repo.RemoveLog(bmiLog.ID);
                if (resultDeletion != null)
                {
                    return RedirectToAction("UserView", "Admin", new { userId = resultDeletion.IdUser });
                }
            }
            ModelState.AddModelError(string.Empty, "Deleting of log has not succeeded");
            _logger.LogWarning(string.Empty, "Attempt to remove the BMI log from the user was unable to be fulfilled.");
            return RedirectToAction("DeleteLogOfUser", "Admin", new { userId = bmiLog.IdUser, id = bmiLog.ID });
        }

        [HttpPost]
        public IActionResult UpdateLogOfUser(BmiLog bmiLog)
        {
            if (ModelState.IsValid)
            {
                if (_repo.GetBmiLogsByUser(bmiLog.IdUser).FirstOrDefault(bmiLog => bmiLog.ID == bmiLog.ID) != null)
                {
                    BmiLog bmiLogUpdated = _repo.UpdateLog(bmiLog);
                    if (bmiLogUpdated != null)
                    {
                        _repo.UpdateLogHistoryUpdate(bmiLogUpdated.ID, UserId());
                        return RedirectToAction("UserView", "Admin", new { userId = bmiLog.IdUser });
                    }
                }
                ModelState.AddModelError(string.Empty, "The edit of the log has failed.");
                _logger.LogError(string.Empty, "The update attempt of the BMI log was unable to be fulfilled.");
            }
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> UpdateUser(UpdateUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                BmiUser user = await _userManager.FindByIdAsync(model.UserId);
                if (user != null)
                {
                    user.UserName = model.NewUserName;
                    IdentityResult result = await _userManager.UpdateAsync(user);
                    if (result.Succeeded)
                    {
                        if (_userManager.IsInRoleAsync(user, Roles.Person).Result)
                        {
                            _repo.UpdateUserHistoryUpdate(user.Id, UserId());
                            return RedirectToAction("UserView", "Admin", new { userId = model.UserId });
                        }
                        if (_userManager.IsInRoleAsync(user, Roles.Coach).Result)
                        {
                            _repo.UpdateUserHistoryUpdate(user.Id, UserId());
                            return RedirectToAction("CoachView", "Admin", new { userId = model.UserId });
                        }
                    }
                    ModelState.AddModelError(string.Empty, "Could not update the username");
                    _logger.LogError("The username of a user couldn't be updated");
                }
            }
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> RemoveUser(BmiUser model)
        {
            if (ModelState.IsValid)
            {
                BmiUser user = await _userManager.FindByEmailAsync(model.Email);
                if (user != null)
                {
                    await _userManager.DeleteAsync(user);
                }
            }
            return RedirectToAction("Index", "Admin");
        }

        [HttpPost]
        public async Task<IActionResult> CreateAuth(AuthRequestsForAdminViewModel model)
        {
            if (ModelState.IsValid)
            {
                BmiUser personGettingRequest = await _userManager.FindByEmailAsync(model.EmailOfRequested);
                BmiUser personSendingRequest = await _userManager.FindByEmailAsync(model.EmailOfTheSender);
                if (personGettingRequest != null && personSendingRequest != null)
                {
                    if (_userManager.IsInRoleAsync(personGettingRequest, Roles.Person).Result &&
                    _userManager.IsInRoleAsync(personSendingRequest, Roles.Coach).Result)
                    {
                        AuthorizedID createdAuth = _repo.CreateAuth(personSendingRequest.Id, personGettingRequest.Id);
                        if (createdAuth != null)
                        {
                            _repo.CreateAuthHistoryInsert(createdAuth.ID, UserId());
                            return RedirectToAction("Index", "Admin");
                        }
                        _logger.LogError(string.Empty, "Could not send an request to get authorization.");
                    }
                }
                ModelState.AddModelError(string.Empty, "Could not get the email of the persons sending and getting.");
                _logger.LogError(string.Empty, "Could not send an request because the user doens't exist or was not in the role of person.");
            }
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> UpdatePasswordUser(EditPasswordOfUserViewModel model)
        {
            if (ModelState.IsValid && model.NewPassword == model.NewPasswordConfirm)
            {
                BmiUser user = await _userManager.FindByIdAsync(model.UserId);
                if (user != null)
                {
                    if (_userManager.UpdatePassword(_logger, user, model.NewPassword).Result.Succeeded)
                    {
                        _repo.UpdateUserHistoryUpdate(user.Id, UserId());
                        return RedirectToAction("Index", "Admin");
                    }
                }
                ModelState.AddModelError(string.Empty, "Given parameters weren't ok.");
            }
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateRolesUser(UserRolesStatusViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.HasAtLeastOneRole())
                {
                    BmiUser user = await _userManager.FindByIdAsync(model.UserId);
                    foreach (RoleOfUserViewModel roleOfUser in model.RolesStatus)
                    {
                        bool isUserInRole = await _userManager.IsInRoleAsync(user, roleOfUser.Name);
                        if (!isUserInRole && roleOfUser.UserInRole)
                        {
                            _repo.UpdateUserHistoryUpdate(user.Id, UserId());
                            await _userManager.AddToRoleAsync(user, roleOfUser.Name);
                        }
                        else if (isUserInRole && !roleOfUser.UserInRole)
                        {
                            _repo.UpdateUserHistoryUpdate(user.Id, UserId());
                            await _userManager.RemoveFromRoleAsync(user, roleOfUser.Name);
                        }
                    }
                    return RedirectToAction("Index", "Admin");
                }
                _logger.LogWarning(string.Empty, "The user has to have at least 1 role.");
                ModelState.AddModelError(string.Empty, "The user has to have at least 1 role.");
            }
            return View(model);
        }

        [HttpPost]
        public IActionResult RemoveAuth(AuthorizedID authorizationFailed)
        {
            if (ModelState.IsValid)
            {
                AuthorizedID authDeleted = _repo.DeleteAuthRequest(authorizationFailed.IdCoach, authorizationFailed.IdUser);
                if (authDeleted != null)
                {
                    return RedirectToAction("CoachView", "Admin", new { userId = authDeleted.IdCoach });
                }
                _logger.LogError(string.Empty, "Could not delete authorization of this user account");
                ModelState.AddModelError(string.Empty, "Could not delete authorization of this user account");
            }
            return View(authorizationFailed);
        }

        [HttpPost]
        public IActionResult ModifyAuthStatus(AuthorizedID auth)
        {
            if (ModelState.IsValid)
            {
                AuthorizedID updatedAuth = _repo.ModifyStatusAuthRequest(auth);
                if (updatedAuth != null)
                {
                    _repo.UpdateAuthHistoryUpdate(updatedAuth.ID, UserId());
                    return RedirectToAction("AuthPageOfUser", "Admin", new { userId = auth.IdUser });
                }
            }
            return View();
        }
        #endregion
    }
}