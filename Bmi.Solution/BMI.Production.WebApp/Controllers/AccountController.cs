﻿using BMI.Production.DAL.Classes;
using BMI.Production.DAL.Interfaces;
using BMI.Production.DAL.Models;
using BMI.Production.WebApp.Classes;
using BMI.Production.WebApp.Interfaces;
using BMI.Production.WebApp.ViewModels.Account;
using BMI.Production.WebApp.ViewModels.Profile;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BMI.Production.WebApp.Controllers
{
    [AllowAnonymous]
    public class AccountController : Controller, IProfileController, IIdentifierController
    {
        #region fields
        private readonly ILogger<AccountController> _logger;
        private readonly UserManager<BmiUser> _userManager;
        private readonly SignInManager<BmiUser> _signInManager;
        private readonly IBmiRepo _repo;
        #endregion

        #region constructor
        public AccountController(ILogger<AccountController> logger, UserManager<BmiUser> userManager,
            SignInManager<BmiUser> signInManager, IBmiRepo repo)
        {
            _logger = logger;
            _userManager = userManager;
            _signInManager = signInManager;
            _repo = repo;
        }
        #endregion

        #region User Info
        [Authorize(Policy = "MustBeSignedIn")]
        public string UserId() => User.FindFirstValue(ClaimTypes.NameIdentifier);

        [Authorize(Policy = "MustBeSignedIn")]
        public string UserEmail() => User.FindFirstValue(ClaimTypes.Email);

        [Authorize(Policy = "MustBeSignedIn")]
        public string UserRole() => User.FindFirstValue(ClaimTypes.Role);
        #endregion

        #region HttpGet-Account
        [HttpGet]
        public IActionResult Login()
        {
            if (_signInManager.IsSignedIn(User))
            {
                return RedirectToAction("Index", UserRole());
            }
            return View();
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpGet]
        [Authorize(Policy = "MustBeSignedIn")]
        public async Task<IActionResult> GetProfileView()
        {
            BmiUser person = await _userManager.GetUserAsync(User) ?? throw new ArgumentNullException();
            return View(person);
        }

        [HttpGet]
        [Authorize(Policy = "MustBeSignedIn")]
        public IActionResult ChangePassword()
        {
            return View();
        }

        [HttpGet]
        [Authorize(Policy = "MustBeSignedIn")]
        public async Task<IActionResult> UpdateProfile()
        {
            BmiUser person = await _userManager.FindByIdAsync(UserId());
            EditNameUserViewModel updateProfileViewModel = new EditNameUserViewModel(person.UserName);
            return View(updateProfileViewModel);
        }
        #endregion

        #region HttpPost-Account
        /**
         * The register function will register an user with his password. A non-admin user can register his account if the email is
         * unique and can't add a account which has already been added even if the admin deleted the account.
         * 
         * Only the admin can overwrite existing and deleted accounts.
         * **/
        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                BmiUser user = _repo.GetByEmailUnFiltered(model.Email);
                IdentityResult identityResult = null;
                if (user == null)
                {
                    identityResult = await _userManager.InsertIfNotPresentAsync(new BmiUser(model.Email), model.Password, _logger);
                    user = await _userManager.FindByEmailAsync(model.Email);
                }
                else if (UserRole() == Roles.Admin)
                {
                    if (_repo.HasUserDeletedFlag(user))
                    {
                        if (_userManager.UpdatePassword(_logger, user, model.Password).Result.Succeeded)
                        {
                            identityResult = IdentityResult.Success;
                        }
                    }
                    else
                    {
                        identityResult = IdentityResult.Failed(new IdentityError[] {
                            new IdentityError() { Description = "You can't add a user with the same email." }
                        });
                    }
                }
                else
                {
                    identityResult = IdentityResult.Failed(new IdentityError[] {
                        new IdentityError() { Description = "You can't add a user with the same email." }
                    });
                }
                if (identityResult.Succeeded)
                {
                    _logger.LogInformation(string.Empty, "An user has been added.");
                    identityResult = await _userManager.ImplementRolesForUserAsync(_logger, user, model.ChosenRoles);
                    if (user != null)
                    {
                        if (_repo.CreateUserHistoryInsert(user.Id, UserId()) != null)
                        {
                            if (UserRole() == Roles.Admin)
                            {
                                return RedirectToAction("Index", "Admin");
                            }
                            await _signInManager.SignInAsync(user, false);
                            IEnumerable<string> roleNameOfUser = await _userManager.GetRolesAsync(user);
                            return RedirectToAction("Index", roleNameOfUser.First());
                        }
                    }
                }
                if (identityResult != null)
                {
                    foreach (IdentityError error in identityResult.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }
            _logger.LogWarning("User registration failed");
            return View(model);
        }

        /**
         * 
         * **/
        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Login", "Account");
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                BmiUser user = await _userManager.FindByEmailAsync(model.Email);
                if (user != null)
                {
                    if (_signInManager.PasswordSignInAsync(user, model.Password, model.RememberMe, false).Result.Succeeded)
                    {
                        if (!string.IsNullOrWhiteSpace(returnUrl) || Url.IsLocalUrl(returnUrl))
                        {
                            return LocalRedirect(returnUrl);
                        }
                        IList<string> rolesOfUser = await _userManager.GetRolesAsync(user);
                        return RedirectToAction("Index", rolesOfUser.First());
                    }
                }
                ModelState.AddModelError(string.Empty, "Invalid Login attempt");
            }
            _logger.LogWarning("Login failed.");
            return View(model);
        }

        /**
         * 
         * **/
        [HttpPost]
        [Authorize(Policy = "MustBeSignedIn")]
        public async Task<IActionResult> UpdateProfile(EditNameUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                BmiUser user = await _userManager.FindByIdAsync(UserId());
                if (user != null)
                {
                    user.UserName = model.UserName;
                    IdentityResult result = await _userManager.UpdateAsync(user);
                    if (result.Succeeded)
                    {
                        if (_repo.UpdateUserHistoryUpdate(user.Id, user.Id) != null)
                        {
                            _logger.LogInformation(string.Empty, "User itself changes its user name.");
                            return RedirectToAction("GetProfileView", "Account");
                        }
                    }
                    _logger.LogError("The username of a user couldn't be updated");
                    ModelState.AddModelError(string.Empty, "Could not update the username");
                }
            }
            return View(model);
        }

        [HttpPost]
        [Authorize(Policy = "MustBeSignedIn")]
        public async Task<IActionResult> ChangePassword(EditPasswordUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                BmiUser user = await _userManager.FindByIdAsync(UserId());
                if (user != null)
                {
                    IdentityResult changePassword = await _userManager
                        .ChangePasswordAsync(user, model.CurrentPassword, model.NewPassword);
                    if (changePassword.Succeeded)
                    {
                        _logger.LogInformation(string.Empty, "User itself changes its password.");
                        if (_repo.UpdateUserHistoryUpdate(user.Id, user.Id) != null)
                        {
                            return RedirectToAction("GetProfileView", "Account");
                        }
                    }
                }
            }
            return View(model);
        }
        #endregion
    }
}