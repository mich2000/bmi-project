﻿using BMI.Production.WebApp.ViewModels.Profile;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BMI.Production.WebApp
{
    /**
     * Interface used to implement methods to get a view of the profile and to modify the profile.
     * 
     * I have created this interface because every role should be able to view and modify his profile.
     * **/
    public interface IProfileController
    {
        [HttpGet]
        [Authorize(Policy = "MustBeSignedIn")]
        public Task<IActionResult> GetProfileView();

        [HttpGet]
        [Authorize(Policy = "MustBeSignedIn")]
        public Task<IActionResult> UpdateProfile();

        [HttpPost]
        [Authorize(Roles = "Admin,Coach,User")]
        public Task<IActionResult> UpdateProfile(EditNameUserViewModel updateProfileViewModel);
    }
}