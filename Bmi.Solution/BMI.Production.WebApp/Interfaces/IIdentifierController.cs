﻿using Microsoft.AspNetCore.Authorization;

namespace BMI.Production.WebApp.Interfaces
{
    /**
     * interface for controller to make methods in controllers so to make user id and email of signed in user available to other
     * methods in the controller.
     * **/
    public interface IIdentifierController
    {
        [Authorize(Policy = "MustBeSignedIn")]
        public string UserId();

        [Authorize(Policy = "MustBeSignedIn")]
        public string UserEmail();

        [Authorize(Policy = "MustBeSignedIn")]
        public string UserRole();
    }
}