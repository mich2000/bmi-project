﻿using System;

namespace BMI.Production.DAL.Classes
{
    public class WebFailure
    {
        #region constructor
        public WebFailure(string errorMessage, string errorStackTrace)
        {
            ErrorMessage = errorMessage;
            ErrorStackTrace = errorStackTrace;
        }

        public WebFailure(Exception exception) : this(exception.Message, exception.StackTrace) { }
        #endregion

        #region properties
        public string ErrorMessage { get; set; }
        public string ErrorStackTrace { get; set; }
        #endregion

        #region methods
        public string LogError() => $"Error {ErrorMessage} was thrown:\nStackTrace: {ErrorStackTrace}";
        #endregion
    }
}