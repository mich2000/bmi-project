﻿namespace BMI.Production.DAL.Classes
{
    public static class Roles
    {
        /**
         * This role will have the authority over the whole system and will be able to access all the data of all the users.
         * **/
        public const string Admin = "Admin";

        /**
         * This role will need the authorization of the user to access his data and follow their evolution. The users where he gets permission to 
         * read their data can export his data as CSV and Markdown file.
         * **/
        public const string Coach = "Coach";

        /**
         * This rol is used for normal people that want to follow their weight and lenght. He will only be able to execute CRUD operations on their
         * own account and data. The user will also be able to export his data as CSV and Markdown file.
         * **/
        public const string Person = "Person";
    }
}