﻿using BMI.Production.DAL.Interfaces;
using System.Net;

namespace BMI.Production.DAL.Classes
{
    /**
     * class used to represent faults of the server that are caused because of the url.
     * **/
    public class FaulthyUrl : IUrl
    {
        #region constructor
        public FaulthyUrl(int statusCode, string path, string parameters)
        {
            StatusCode = statusCode;
            Path = path;
            Parameters = parameters;
        }
        #endregion

        #region properties
        public int StatusCode { get; set; }
        public string Path { get; set; }
        public string Parameters { get; set; }
        #endregion

        #region methods
        public string ErrorLog() => $"{StatusCode} error occured. Path = {Path} and Querystring = {Parameters}";

        /**
         * gives back a message based on the httpcode.
         * **/
        public string StatusCodeMessage()
        {
            return StatusCode switch
            {
                (int)HttpStatusCode.NotFound => "Sorry, the ressource you requested could not be found.",
                (int)HttpStatusCode.Forbidden => "Sorry, but you are not authorized to access this part of the website",
                _ => "Status code has not be found or implemented yet",
            };
        }
        #endregion
    }
}