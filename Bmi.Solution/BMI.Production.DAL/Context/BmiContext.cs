﻿using BMI.Production.DAL.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BMI.Production.DAL.Context
{
    public class BmiContext : IdentityDbContext<BmiUser>
    {
        public BmiContext(DbContextOptions<BmiContext> options) : base(options) { }

        public DbSet<BmiLog> BmiLogs { get; set; }
        public DbSet<AuthorizedID> AuthorizedIDs { get; set; }

        /**
         * foreign key modeling: https://docs.microsoft.com/en-us/ef/core/modeling/relationships?tabs=fluent-api%2Cfluent-api-simple-key%2Csimple-key
         * Soft delete implementation: https://spin.atomicobject.com/2019/01/29/entity-framework-core-soft-delete/
         * **/
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            //foreign key binding the IdUser that identifies from who is this Bmi log.
            builder.Entity<BmiUser>().Property<bool>("isDeleted");
            builder.Entity<BmiUser>().HasQueryFilter(m => EF.Property<bool>(m, "isDeleted") == false);

            builder.Entity<BmiLog>().Property<bool>("isDeleted");
            builder.Entity<BmiLog>().HasQueryFilter(m => EF.Property<bool>(m, "isDeleted") == false);

            builder.Entity<AuthorizedID>().Property<bool>("isDeleted");
            builder.Entity<AuthorizedID>().HasQueryFilter(m => EF.Property<bool>(m, "isDeleted") == false);

            builder.Entity<BmiLog>()
                .HasOne<BmiUser>()
                .WithMany()
                .HasForeignKey(key => key.IdUser);
            //foreign key binding the IdUser that identifies to who the authorization request was send
            //foreign key binding the IdCoach that identifies from who the authorization request was send
            builder.Entity<AuthorizedID>()
                .HasOne<BmiUser>()
                .WithMany()
                .HasForeignKey(key => key.IdUser);
        }

        public override int SaveChanges()
        {
            foreach (var entry in ChangeTracker.Entries())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.CurrentValues["isDeleted"] = false;
                        break;
                    case EntityState.Deleted:
                        entry.State = EntityState.Modified;
                        entry.CurrentValues["isDeleted"] = true;
                        break;
                }
            }
            return base.SaveChanges();
        }
    }
}