﻿using BMI.Production.DAL.Context;
using BMI.Production.DAL.Interfaces;
using BMI.Production.DAL.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BMI.Production.DAL.Repositories
{
    /**
     * Class representing the repository used for CRUD operations for user.
     * **/
    public class BmiRepo : IBmiRepo
    {
        #region fields
        private readonly BmiContext _context;
        #endregion

        #region constructor
        public BmiRepo(BmiContext context)
        {
            _context = context;
        }
        #endregion

        #region user
        public IEnumerable<BmiUser> GetAllUsers()
        {
            return _context.Users.AsNoTracking().AsEnumerable();
        }

        public BmiUser GetById(string id)
        {
            return _context.Users.AsNoTracking().FirstOrDefault(user => user.Id == id);
        }

        public BmiUser GetByEmailUnFiltered(string email)
        {
            return _context.Users.AsNoTracking().IgnoreQueryFilters().FirstOrDefault(user => user.Email == email);
        }

        public bool RemoveDeletedFlagOfUser(BmiUser user)
        {
            if (user != null)
            {
                _context.Entry(user).CurrentValues["isDeleted"] = false;
                return true;
            }
            return false;
        }

        public bool HasUserDeletedFlag(BmiUser user)
        {
            if (user != null)
            {
                BmiUser bmiUser = GetByEmailUnFiltered(user.Email);
                string isdeleted = _context.Entry(bmiUser).CurrentValues["isDeleted"].ToString();
                return isdeleted == "True";
            }
            return true;
        }
        #endregion

        #region authorizedIDs
        public AuthorizedID CreateAuth(string coach_id, string user_id)
        {
            if (!string.IsNullOrEmpty(coach_id) && !string.IsNullOrEmpty(user_id)
                && !_context.AuthorizedIDs.Any(auth => auth.IdCoach == coach_id && auth.IdUser == user_id))
            {
                AuthorizedID authorizedID = new AuthorizedID(user_id, coach_id);
                EntityEntry<AuthorizedID> entityEntryAuthId = _context.AuthorizedIDs.Add(authorizedID);
                if (entityEntryAuthId != null && entityEntryAuthId.State == EntityState.Added)
                {
                    int affectedRows = _context.SaveChanges();
                    if (affectedRows > 0)
                    {
                        return _context.Entry(authorizedID).Entity;
                    }
                }
            }
            return null;
        }

        public AuthorizedID ModifyStatusAuthRequest(AuthorizedID auth)
        {
            if (auth != null)
            {
                EntityEntry<AuthorizedID> entityEntryAuthId = _context.AuthorizedIDs.Update(auth);
                if (entityEntryAuthId != null && entityEntryAuthId.State == EntityState.Modified)
                {
                    int affectedRows = _context.SaveChanges();
                    if (affectedRows > 0)
                    {
                        return _context.Entry(auth).Entity;
                    }
                }
            }
            return null;
        }

        public AuthorizedID GetSpecificAuthRequest(string coach_id, string user_id)
        {
            return _context.AuthorizedIDs.AsNoTracking().FirstOrDefault(auth => auth.IdUser == user_id && auth.IdCoach == coach_id);
        }

        public AuthorizedID DeleteAuthRequest(string coach_id, string user_id)
        {
            if (!string.IsNullOrEmpty(coach_id) && !string.IsNullOrEmpty(user_id))
            {
                AuthorizedID authToBeDeleted = GetSpecificAuthRequest(coach_id, user_id);
                EntityEntry<AuthorizedID> entityEntryAuthId = _context.AuthorizedIDs.Remove(authToBeDeleted);
                if (entityEntryAuthId != null && entityEntryAuthId.State == EntityState.Deleted)
                {
                    int affectedRows = _context.SaveChanges();
                    if (affectedRows > 0)
                    {
                        return _context.Entry(authToBeDeleted).Entity;
                    }
                }
            }
            return null;
        }

        public IEnumerable<AuthorizedID> GetAuthRequestFromUser(string user_id)
        {
            return _context.AuthorizedIDs.AsNoTracking().Where(auth => auth.IdUser == user_id).AsEnumerable();
        }

        public AuthorizedID GetAuthById(int id)
        {
            return _context.AuthorizedIDs.AsNoTracking().FirstOrDefault(auth => auth.ID == id);
        }

        public IEnumerable<AuthorizedID> GetAuthRequestFromCoach(string user_coach)
        {
            return _context.AuthorizedIDs.AsNoTracking().Where(auth => auth.IdCoach == user_coach).AsEnumerable();
        }
        #endregion

        #region BMIs
        public BmiLog InsertLog(BmiLog bmiLog)
        {
            if (bmiLog != null)
            {
                EntityEntry<BmiLog> entityEntryAuthId = _context.BmiLogs.Add(bmiLog);
                if (entityEntryAuthId != null && entityEntryAuthId.State == EntityState.Added)
                {
                    int affectedRows = _context.SaveChanges();
                    if (affectedRows > 0)
                    {
                        return _context.Entry(bmiLog).Entity;
                    }
                }
            }
            return null;
        }

        public BmiLog UpdateLog(BmiLog bmiLog)
        {
            if (bmiLog != null)
            {
                EntityEntry<BmiLog> entityEntryAuthId = _context.BmiLogs.Update(bmiLog);
                if (entityEntryAuthId != null && entityEntryAuthId.State == EntityState.Modified)
                {
                    int affectedRows = _context.SaveChanges();
                    if (affectedRows > 0)
                    {
                        return _context.Entry(bmiLog).Entity;
                    }
                }
            }
            return null;
        }

        public BmiLog RemoveLog(int bmiLogId)
        {
            BmiLog logToBeDeleted = _context.BmiLogs.AsNoTracking().FirstOrDefault(log => log.ID == bmiLogId);
            EntityEntry<BmiLog> entityEntryAuthId = _context.BmiLogs.Remove(logToBeDeleted);
            if (entityEntryAuthId != null && entityEntryAuthId.State == EntityState.Deleted)
            {
                int affectedRows = _context.SaveChanges();
                if (affectedRows > 0)
                {
                    return _context.Entry(logToBeDeleted).Entity;
                }
            }
            return null;
        }

        public BmiLog GetBmiLog(string user_id, int id_bmi_log)
        {
            return _context.BmiLogs.AsNoTracking().FirstOrDefault(log => log.IdUser == user_id && log.ID == id_bmi_log);
        }

        public BmiLog GetBmiLogById(int id)
        {
            return _context.BmiLogs.AsNoTracking().FirstOrDefault(log => log.ID == id);
        }

        public IEnumerable<BmiLog> GetBmiLogsByUser(string user_id)
        {
            return _context.BmiLogs.AsNoTracking().Where(log => log.IdUser == user_id).AsEnumerable();
        }
        #endregion

        #region History
        public BmiUser CreateUserHistoryInsert(string user_id, string creator_id)
        {
            BmiUser user = _context.Users.Find(user_id);
            if (user != null)
            {
                user.CreatedBy = creator_id;
                user.CreatedOn = DateTime.UtcNow;
                int affectedRows = _context.SaveChanges();
                if (affectedRows > 0)
                {
                    return _context.Entry(user).Entity;
                }
            }
            return null;
        }

        public BmiUser UpdateUserHistoryUpdate(string user_id, string update_id)
        {
            BmiUser user = _context.Users.Find(user_id);
            if (user != null)
            {
                user.UpdatedBy = update_id;
                user.UpdatedOn = DateTime.UtcNow;
                int affectedRows = _context.SaveChanges();
                if (affectedRows > 0)
                {
                    return _context.Entry(user).Entity;
                }
            }
            return null;
        }

        public BmiLog CreateLogHistoryInsert(int id, string creator_id)
        {
            BmiLog log = _context.BmiLogs.Find(id);
            if (log != null)
            {
                log.CreatedBy = creator_id;
                log.CreatedOn = DateTime.UtcNow;
                int affectedRows = _context.SaveChanges();
                if (affectedRows > 0)
                {
                    return _context.Entry(log).Entity;
                }
            }
            return null;
        }

        public BmiLog UpdateLogHistoryUpdate(int id, string update_id)
        {
            BmiLog log = _context.BmiLogs.Find(id);
            if (log != null)
            {
                log.UpdatedBy = update_id;
                log.UpdatedOn = DateTime.UtcNow;
                int affectedRows = _context.SaveChanges();
                if (affectedRows > 0)
                {
                    return _context.Entry(log).Entity;
                }
            }
            return null;
        }

        public AuthorizedID CreateAuthHistoryInsert(int id, string creator_id)
        {
            AuthorizedID auth = _context.AuthorizedIDs.Find(id);
            if (auth != null)
            {
                auth.CreatedBy = creator_id;
                auth.CreatedOn = DateTime.UtcNow;
                int affectedRows = _context.SaveChanges();
                if (affectedRows > 0)
                {
                    return _context.Entry(auth).Entity;
                }
            }
            return null;
        }

        public AuthorizedID UpdateAuthHistoryUpdate(int id, string update_id)
        {
            AuthorizedID auth = _context.AuthorizedIDs.Find(id);
            if (auth != null)
            {
                auth.UpdatedBy = update_id;
                auth.UpdatedOn = DateTime.UtcNow;
                int affectedRows = _context.SaveChanges();
                if (affectedRows > 0)
                {
                    return _context.Entry(auth).Entity;
                }
            }
            return null;
        }
        #endregion
    }
}