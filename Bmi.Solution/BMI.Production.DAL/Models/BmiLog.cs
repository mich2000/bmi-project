﻿using BMI.Production.DAL.Classes;
using BMI.Production.DAL.Enums;
using BMI.Production.DAL.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;

namespace BMI.Production.DAL.Models
{
    /**
     * Class used to represent the representation of a bmi data class and also with the ability to document the date the measuring
     * happened.
     * **/
    public class BmiLog : IBmi, IPersonLog, IHistory
    {
        #region fields
        private const double idealBmiConst = 21.75;
        private decimal _lenght;
        private decimal _weight;
        private DateTime _dateLog;
        #endregion

        #region constructor
        public BmiLog(int iD, string userId, DateTime dateLog, decimal lenght, decimal weight)
        {
            ID = iD;
            IdUser = userId;
            DateLog = dateLog;
            Lenght = lenght;
            Weight = weight;
        }

        public BmiLog(DateTime dateLog, string idUser, decimal lenght, decimal weight)
            : this(0, idUser, dateLog, lenght, weight) { }

        public BmiLog(decimal lenght, decimal weight)
            : this(0, null, DateTime.Now, lenght, weight) { }

        public BmiLog() : this(0.0M, 0.0M) { }
        #endregion

        #region properties
        [Key]
        public int ID { get; set; }

        [Required]
        public string IdUser { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Date measuring happened: ")]
        public DateTime DateLog
        {
            get { return _dateLog; }
            set
            {
                if (value != _dateLog) _dateLog = value;
            }
        }

        [Required]
        [Range(0, 9999999999999999.99, ErrorMessage = "Please enter valid decimalNumber, seperated by a dot.")]
        [Display(Name = "Length of the person: ")]
        public decimal Lenght
        {
            get { return _lenght; }
            set
            {
                if (value != _lenght) _lenght = value.To2Decimals();
            }
        }

        [Required]
        [Range(0, 9999999999999999.99, ErrorMessage = "Please enter valid decimalNumber, seperated by a dot.")]
        [Display(Name = "Weight of the person: ")]
        public decimal Weight
        {
            get { return _weight; }
            set
            {
                if (value != _weight) _weight = value.To2Decimals();
            }
        }

        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        #endregion

        #region methods
        public decimal BMI()
        {
            return (_weight / (_lenght * _lenght)).To2Decimals();
        }

        public decimal BSA()
        {
            return (0.007184M * (decimal)Math.Pow((double)Weight, 0.425) * (decimal)Math.Pow((double)Lenght * 100.0, 0.725)).To2Decimals();
        }

        public decimal IdealWeight()
        {
            return ((decimal)idealBmiConst * _lenght * _lenght).To2Decimals();
        }

        public BmiCategory BMIcategory()
        {
            decimal bmi = BMI();
            if (bmi < 18.5M) return BmiCategory.UnderWeight;
            else if (bmi >= 18.5M && bmi < 25) return BmiCategory.NormalWeight;
            else if (bmi >= 25 && bmi < 30) return BmiCategory.OverWeight;
            else return BmiCategory.Obesitas;
        }
        #endregion
    }
}