﻿using BMI.Production.DAL.Enums;
using BMI.Production.DAL.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;

namespace BMI.Production.DAL.Models
{
    /**
     * class representing a authorization, this is valid for only user from whom you ask the authorization.
     * 
     * When sending a request to be authorized to read their data, the status is set to Pending and after the user accepts the request it is 
     * set to Accepted. If the user refuses the request it is set to failed.
     * **/
    public class AuthorizedID : IHistory
    {
        #region constructors
        public AuthorizedID(string user_id, string id_coach)
        {
            IdUser = user_id;
            IdCoach = id_coach;
            StatusRequest = Status.Pending;
        }

        public AuthorizedID() { }
        #endregion

        #region properties
        [Key]
        public int ID { get; set; }

        [Required]
        public string IdUser { get; set; }

        [Required]
        [Display(Name = "ID coach: ")]
        public string IdCoach { get; set; }

        [Required]
        [Display(Name = "Status of the authorization: ")]
        public Status StatusRequest { get; set; }

        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        #endregion
    }
}