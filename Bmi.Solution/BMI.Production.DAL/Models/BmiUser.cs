﻿using BMI.Production.DAL.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;

namespace BMI.Production.DAL.Models
{
    public class BmiUser : IdentityUser, IHistory
    {
        public BmiUser(string email)
        {
            Email = email;
            UserName = email;
        }

        public BmiUser() { }

        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
    }
}