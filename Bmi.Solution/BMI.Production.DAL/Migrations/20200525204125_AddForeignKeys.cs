﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BMI.Production.DAL.Migrations
{
    public partial class AddForeignKeys : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "IdUser",
                table: "BmiLogs",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<string>(
                name: "IdCoach",
                table: "AuthorizedIDs",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.CreateIndex(
                name: "IX_BmiLogs_IdUser",
                table: "BmiLogs",
                column: "IdUser");

            migrationBuilder.CreateIndex(
                name: "IX_AuthorizedIDs_IdCoach",
                table: "AuthorizedIDs",
                column: "IdCoach");

            migrationBuilder.AddForeignKey(
                name: "FK_AuthorizedIDs_AspNetUsers_IdCoach",
                table: "AuthorizedIDs",
                column: "IdCoach",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BmiLogs_AspNetUsers_IdUser",
                table: "BmiLogs",
                column: "IdUser",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AuthorizedIDs_AspNetUsers_IdCoach",
                table: "AuthorizedIDs");

            migrationBuilder.DropForeignKey(
                name: "FK_BmiLogs_AspNetUsers_IdUser",
                table: "BmiLogs");

            migrationBuilder.DropIndex(
                name: "IX_BmiLogs_IdUser",
                table: "BmiLogs");

            migrationBuilder.DropIndex(
                name: "IX_AuthorizedIDs_IdCoach",
                table: "AuthorizedIDs");

            migrationBuilder.AlterColumn<string>(
                name: "IdUser",
                table: "BmiLogs",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "IdCoach",
                table: "AuthorizedIDs",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string));
        }
    }
}
