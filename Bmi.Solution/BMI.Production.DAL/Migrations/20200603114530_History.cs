﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace BMI.Production.DAL.Migrations
{
    public partial class History : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AuthorizedIDs_AspNetUsers_IdCoach",
                table: "AuthorizedIDs");

            migrationBuilder.DropIndex(
                name: "IX_AuthorizedIDs_IdCoach",
                table: "AuthorizedIDs");

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "BmiLogs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOn",
                table: "BmiLogs",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "UpdatedBy",
                table: "BmiLogs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedOn",
                table: "BmiLogs",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AlterColumn<string>(
                name: "IdUser",
                table: "AuthorizedIDs",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<string>(
                name: "IdCoach",
                table: "AuthorizedIDs",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)");

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "AuthorizedIDs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOn",
                table: "AuthorizedIDs",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "UpdatedBy",
                table: "AuthorizedIDs",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedOn",
                table: "AuthorizedIDs",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.CreateIndex(
                name: "IX_AuthorizedIDs_IdUser",
                table: "AuthorizedIDs",
                column: "IdUser");

            migrationBuilder.AddForeignKey(
                name: "FK_AuthorizedIDs_AspNetUsers_IdUser",
                table: "AuthorizedIDs",
                column: "IdUser",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AuthorizedIDs_AspNetUsers_IdUser",
                table: "AuthorizedIDs");

            migrationBuilder.DropIndex(
                name: "IX_AuthorizedIDs_IdUser",
                table: "AuthorizedIDs");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "BmiLogs");

            migrationBuilder.DropColumn(
                name: "CreatedOn",
                table: "BmiLogs");

            migrationBuilder.DropColumn(
                name: "UpdatedBy",
                table: "BmiLogs");

            migrationBuilder.DropColumn(
                name: "UpdatedOn",
                table: "BmiLogs");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "AuthorizedIDs");

            migrationBuilder.DropColumn(
                name: "CreatedOn",
                table: "AuthorizedIDs");

            migrationBuilder.DropColumn(
                name: "UpdatedBy",
                table: "AuthorizedIDs");

            migrationBuilder.DropColumn(
                name: "UpdatedOn",
                table: "AuthorizedIDs");

            migrationBuilder.AlterColumn<string>(
                name: "IdUser",
                table: "AuthorizedIDs",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "IdCoach",
                table: "AuthorizedIDs",
                type: "nvarchar(450)",
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.CreateIndex(
                name: "IX_AuthorizedIDs_IdCoach",
                table: "AuthorizedIDs",
                column: "IdCoach");

            migrationBuilder.AddForeignKey(
                name: "FK_AuthorizedIDs_AspNetUsers_IdCoach",
                table: "AuthorizedIDs",
                column: "IdCoach",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
