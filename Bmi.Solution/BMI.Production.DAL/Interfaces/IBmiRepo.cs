﻿using BMI.Production.DAL.Models;
using System.Collections.Generic;

namespace BMI.Production.DAL.Interfaces
{
    public interface IBmiRepo
    {
        #region user
        IEnumerable<BmiUser> GetAllUsers();
        BmiUser GetById(string id);
        BmiUser GetByEmailUnFiltered(string email);
        bool RemoveDeletedFlagOfUser(BmiUser user);
        bool HasUserDeletedFlag(BmiUser user);
        #endregion

        #region authorizedIDs
        AuthorizedID CreateAuth(string coach_id, string user_id);
        AuthorizedID ModifyStatusAuthRequest(AuthorizedID auth);
        AuthorizedID GetSpecificAuthRequest(string coach_id, string user_id);
        AuthorizedID DeleteAuthRequest(string coach_id, string user_id);
        AuthorizedID GetAuthById(int id);
        IEnumerable<AuthorizedID> GetAuthRequestFromUser(string user_id);
        IEnumerable<AuthorizedID> GetAuthRequestFromCoach(string user_coach);
        #endregion

        #region BMIs
        BmiLog InsertLog(BmiLog bmiLog);
        BmiLog UpdateLog(BmiLog bmiLog);
        BmiLog RemoveLog(int bmiLogId);
        BmiLog GetBmiLog(string user_id, int id_bmi_log);
        BmiLog GetBmiLogById(int id);
        IEnumerable<BmiLog> GetBmiLogsByUser(string user_id);
        #endregion

        #region History
        BmiUser CreateUserHistoryInsert(string user_id, string creator_id);
        BmiUser UpdateUserHistoryUpdate(string user_id, string update_id);
        BmiLog CreateLogHistoryInsert(int id, string creator_id);
        BmiLog UpdateLogHistoryUpdate(int id, string update_id);
        AuthorizedID CreateAuthHistoryInsert(int id, string creator_id);
        AuthorizedID UpdateAuthHistoryUpdate(int id, string update_id);
        #endregion
    }
}