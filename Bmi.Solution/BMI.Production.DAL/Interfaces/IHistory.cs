﻿using System;

namespace BMI.Production.DAL.Interfaces
{
    /**
     * interface used to implement properties used to keep a track on the person who created and updated and to track the dates 
     * it did on.
     * **/
    public interface IHistory
    {
        public string CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public string UpdatedBy { get; set; }

        public DateTime UpdatedOn { get; set; }
    }
}