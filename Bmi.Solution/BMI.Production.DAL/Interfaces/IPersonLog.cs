﻿using System;

namespace BMI.Production.DAL.Interfaces
{
    /**
     * interface used to implement a log that will have to get a DateLog.
     * **/
    public interface IPersonLog
    {
        public int ID { get; set; }
        public DateTime DateLog { get; set; }
    }
}