﻿using BMI.Production.DAL.Enums;

namespace BMI.Production.DAL.Interfaces
{
    /**
     * Interface that represents how a data class that can return the bmi, bsa and ideal weight should be.
     * The 2 most important properties are Lenght and Weight.
     * **/
    public interface IBmi
    {
        public decimal Lenght { get; set; }
        public decimal Weight { get; set; }
        public decimal BMI();
        public decimal BSA();
        public decimal IdealWeight();
        public BmiCategory BMIcategory();
    }
}