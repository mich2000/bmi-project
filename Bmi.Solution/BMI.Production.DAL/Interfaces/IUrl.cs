﻿using System;

namespace BMI.Production.DAL.Interfaces
{
    public interface IUrl
    {
        int StatusCode { get; set; }
        String Path { get; set; }
        String Parameters { get; set; }
        String StatusCodeMessage();
        String ErrorLog();
    }
}