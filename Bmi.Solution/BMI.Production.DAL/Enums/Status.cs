﻿namespace BMI.Production.DAL.Enums
{
    public enum Status { Accepted, Pending, Failed }
}