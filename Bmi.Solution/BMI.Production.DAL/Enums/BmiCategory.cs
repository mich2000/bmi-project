﻿namespace BMI.Production.DAL.Enums
{
    public enum BmiCategory { UnderWeight, NormalWeight, OverWeight, Obesitas }
}