using System;
using System.IO;
using System.Runtime.InteropServices;
using BMI.Production.DAL.Context;
using BMI.Production.DAL.Interfaces;
using BMI.Production.DAL.Models;
using BMI.Production.DAL.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace BMI.Production.SqlLite.WebApp
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                Directory.CreateDirectory("D://BmiSqlite");
                services.AddDbContextPool<BmiContext>(options =>
                {
                    options.UseSqlite(Configuration.GetConnectionString("BmiDbConnection"));
                });
            }
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                Directory.CreateDirectory("/BmiSqlite");
                services.AddDbContextPool<BmiContext>(options =>
                {
                    options.UseSqlite(Configuration.GetConnectionString("BmiDbConnectionLinux"));
                });
            }
            services.AddIdentity<BmiUser, IdentityRole>(options =>
            {
                options.Password.RequiredLength = 4;
                options.Password.RequiredUniqueChars = 1;
                options.Password.RequireDigit = false;
                options.Password.RequireNonAlphanumeric = false;
                options.SignIn.RequireConfirmedAccount = false;
                //makes sure there are only uniques emails
                options.User.RequireUniqueEmail = true;
                //will skip user name validation
                options.User.AllowedUserNameCharacters = string.Empty;
            })
            .AddEntityFrameworkStores<BmiContext>();
            services.AddControllersWithViews(config =>
            {
                config.Filters.Add(new AuthorizeFilter(new AuthorizationPolicyBuilder().RequireAuthenticatedUser().Build()));
            });
            services.AddAuthorization(options =>
            {
                options.AddPolicy("MustBeSignedIn", policy => policy.RequireAuthenticatedUser());
            });
            services.AddScoped<IBmiRepo, BmiRepo>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider serviceProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseStatusCodePagesWithReExecute("/Error/{0}");
            }

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(name: "default", pattern: "{controller=Account}/{action=Login}/{id?}");
            });
            serviceProvider.SeedUsersAndRoles().Wait();
            //serviceProvider.SeedPersonsAndCoaches().Wait();
        }
    }
}
