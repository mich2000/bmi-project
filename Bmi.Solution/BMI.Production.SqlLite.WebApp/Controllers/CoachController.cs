﻿using BMI.Production.DAL.Classes;
using BMI.Production.DAL.Enums;
using BMI.Production.DAL.Interfaces;
using BMI.Production.DAL.Models;
using BMI.Production.SqlLite.WebApp.Interfaces;
using BMI.Production.SqlLite.WebApp.ViewModels.Coach;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BMI.Production.SqlLite.WebApp.Controllers
{
    [Authorize(Roles = Roles.Admin + "," + Roles.Coach)]
    [Authorize(Policy = "MustBeSignedIn")]
    public class CoachController : Controller, IIdentifierController
    {
        #region fields
        private readonly IBmiRepo _repo;
        private readonly UserManager<BmiUser> _userManager;
        private readonly ILogger<CoachController> _logger;
        #endregion

        #region User Info
        [Authorize(Policy = "MustBeSignedIn")]
        public string UserId() => User.FindFirstValue(ClaimTypes.NameIdentifier);

        [Authorize(Policy = "MustBeSignedIn")]
        public string UserEmail() => User.FindFirstValue(ClaimTypes.Email);

        [Authorize(Policy = "MustBeSignedIn")]
        public string UserRole() => User.FindFirstValue(ClaimTypes.Role);
        #endregion

        #region constructors
        public CoachController(IBmiRepo repo, UserManager<BmiUser> userManager, ILogger<CoachController> logger)
        {
            _repo = repo;
            _userManager = userManager;
            _logger = logger;
        }
        #endregion

        #region HttpGet - Coach
        [HttpGet]
        public IActionResult Index()
        {
            string userId = UserId();
            IEnumerable<AuthorizedID> requests = _repo.GetAuthRequestFromCoach(userId);
            RequestViewModel requestViewModel = new RequestViewModel(requests.Where(auth => auth.StatusRequest == Status.Accepted),
                requests.Where(auth => auth.StatusRequest != Status.Accepted));
            return View(requestViewModel);
        }

        [HttpGet]
        public IActionResult MakeRequest()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> RemoveAuth(string userId)
        {
            BmiUser person = await _userManager.FindByIdAsync(userId) ?? throw new ArgumentNullException();
            return View(person);
        }

        [HttpGet]
        public IActionResult UserView(string userId)
        {
            IEnumerable<BmiLog> bmiLogs = _repo.GetBmiLogsByUser(userId) ?? throw new ArgumentNullException();
            UserViewViewModel userViewViewModel = new UserViewViewModel(bmiLogs, userId);
            return View(userViewViewModel);
        }

        [HttpGet]
        public IActionResult DetailBmiLogOfUser(string userId, int bmiLogId)
        {
            string coachId = UserId();
            AuthorizedID authorizedID = _repo.GetSpecificAuthRequest(coachId, userId);
            if (authorizedID != null)
            {
                BmiLog bmiLogUser = _repo.GetBmiLog(authorizedID.IdUser, bmiLogId);
                string IdUser = _repo.GetById(userId).Id;
                return View(new DetailBmiLogOfUserViewModel(IdUser, bmiLogUser));
            }
            return RedirectToAction("Index", "Coach");
        }
        #endregion

        #region HttpPost - Coach
        [HttpPost]
        public async Task<IActionResult> MakeRequest(AuthRequestViewModel model)
        {
            if (ModelState.IsValid)
            {
                BmiUser person = await _userManager.FindByEmailAsync(model.EmailOfRequested);
                if(person != null)
                {
                    if (_userManager.IsInRoleAsync(person, Roles.Person).Result && person.Id != UserId())
                    {
                        AuthorizedID sendAuthRequest = _repo.CreateAuth(UserId(), person.Id);
                        if (sendAuthRequest != null)
                        {
                            _repo.CreateAuthHistoryInsert(sendAuthRequest.ID, UserId());
                            return RedirectToAction("Index", "Coach");
                        }
                        ModelState.AddModelError(string.Empty, "Could not send an request to get authorization.");
                        _logger.LogError(string.Empty, "Could not send an request to get authorization.");
                    }
                }
                ModelState.AddModelError(string.Empty, "Could not send an request because the user doens't exist or was not in the role of person.");
                _logger.LogError(string.Empty, "Could not send an request because the user doens't exist or was not in the role of person.");
            }
            return View(model);
        }

        [HttpPost]
        public IActionResult RemoveAuthUser(BmiUser user)
        {
            if (!string.IsNullOrEmpty(user.Id))
            {
                string coachId = UserId();
                AuthorizedID removedAuthRequest = _repo.DeleteAuthRequest(coachId, user.Id);
                if (removedAuthRequest != null)
                {
                    return RedirectToAction("Index", "Coach");
                }
                _logger.LogError(string.Empty, "Could not delete authorization to an user account");
            }
            return RedirectToAction("Index", "Coach");
        }
        #endregion
    }
}