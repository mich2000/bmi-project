﻿using BMI.Production.DAL.Classes;
using BMI.Production.DAL.Enums;
using BMI.Production.DAL.Interfaces;
using BMI.Production.DAL.Models;
using BMI.Production.SqlLite.WebApp.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BMI.Production.SqlLite.WebApp.Controllers
{
    [Authorize(Roles = Roles.Admin + "," + Roles.Person)]
    [Authorize(Policy = "MustBeSignedIn")]
    public class PersonController : Controller, IIdentifierController
    {
        #region fields
        private readonly IBmiRepo _repo;
        private readonly UserManager<BmiUser> _userManager;
        private readonly ILogger<PersonController> _logger;
        #endregion

        #region User Info
        [Authorize(Policy = "MustBeSignedIn")]
        public string UserId() => User.FindFirstValue(ClaimTypes.NameIdentifier);

        [Authorize(Policy = "MustBeSignedIn")]
        public string UserEmail() => User.FindFirstValue(ClaimTypes.Email);

        [Authorize(Policy = "MustBeSignedIn")]
        public string UserRole() => User.FindFirstValue(ClaimTypes.Role);
        #endregion

        #region constructors
        public PersonController(IBmiRepo repo, UserManager<BmiUser> userManager, ILogger<PersonController> logger)
        {
            _repo = repo;
            _userManager = userManager;
            _logger = logger;
        }
        #endregion

        #region HttpGet - Person
        [HttpGet]
        public IActionResult Index()
        {
            IEnumerable<BmiLog> bmiLogs = _repo.GetBmiLogsByUser(UserId());
            return View(bmiLogs);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            BmiLog log = _repo.GetBmiLog(UserId(), id);
            return View(log);
        }

        [HttpGet]
        public IActionResult Details(int ID)
        {
            BmiLog log = _repo.GetBmiLog(UserId(), ID);
            return View(log);
        }

        [HttpGet]
        public IActionResult Update(int id)
        {
            BmiLog log = _repo.GetBmiLog(UserId(), id);
            return View(log);
        }

        [HttpGet]
        public async Task<IActionResult> DecideOverRequest(string IdCoach)
        {
            BmiUser Coach = await _userManager.FindByIdAsync(IdCoach) ?? throw new ArgumentNullException();
            AuthorizedID authorizedID = _repo.GetSpecificAuthRequest(Coach.Id, UserId()) ?? throw new ArgumentNullException();
            return View(authorizedID);
        }

        [HttpGet]
        public IActionResult Authorizations()
        {
            IEnumerable<AuthorizedID> authorizedIDs = _repo.GetAuthRequestFromUser(UserId());
            return View(authorizedIDs);
        }
        #endregion

        #region HttpPost - Person
        [HttpPost]
        public IActionResult Create(BmiLog log)
        {
            if (ModelState.IsValid)
            {
                BmiLog createdLog = _repo.InsertLog(new BmiLog(log.DateLog, UserId(), log.Lenght, log.Weight));
                if (createdLog != null)
                {
                    _repo.CreateLogHistoryInsert(createdLog.ID, UserId());
                    return RedirectToAction("Index", "Person");
                }
                ModelState.AddModelError(string.Empty, "Adding bmi log has not succeeded");
                _logger.LogWarning(string.Empty, "Could not create a BMI log linked to an user.");
            }
            return View();
        }

        [HttpPost]
        public IActionResult DeleteLog(int ID)
        {
            BmiLog bmiLogToBeDeleted = _repo.GetBmiLogsByUser(UserId()).FirstOrDefault(log => log.ID == ID);
            if (bmiLogToBeDeleted != null)
            {
                BmiLog resultDeletion = _repo.RemoveLog(ID) ?? throw new ArgumentNullException();
                if (resultDeletion != null)
                {
                    return RedirectToAction("Index", "Person");
                }
            }
            ModelState.AddModelError(string.Empty, "Deleting of log has not succeeded");
            _logger.LogWarning(string.Empty, "Attempt to remove the BMI log from the user was unable to be fulfilled.");
            return View();
        }

        [HttpPost]
        public IActionResult Update(BmiLog log)
        {
            if (ModelState.IsValid)
            {
                if (_repo.GetBmiLogsByUser(UserId()).FirstOrDefault(bmiLog => bmiLog.ID == log.ID) != null)
                {
                    BmiLog updatedLog = _repo.UpdateLog(log);
                    if (updatedLog != null)
                    {
                        _repo.UpdateLogHistoryUpdate(updatedLog.ID, UserId());
                        return RedirectToAction("details", "Person", new { updatedLog.ID });
                    }
                }
                ModelState.AddModelError(string.Empty, "The edit of the log has failed.");
                _logger.LogWarning(string.Empty, "The update attempt of the BMI log was unable to be fulfilled.");
            }
            return View();
        }

        [HttpPost]
        public IActionResult AcceptRequest(AuthorizedID model)
        {
            if (!string.IsNullOrEmpty(model.IdCoach) &&  ModelState.IsValid)
            {
                AuthorizedID AcceptedRequest = _repo.GetSpecificAuthRequest(model.IdCoach, UserId());
                AcceptedRequest.StatusRequest = Status.Accepted;
                AuthorizedID acceptedAuthRequest = _repo.ModifyStatusAuthRequest(AcceptedRequest);
                if (acceptedAuthRequest != null)
                {
                    _repo.UpdateAuthHistoryUpdate(acceptedAuthRequest.ID, UserId());
                    return RedirectToAction("Authorizations", "Person");
                }
                ModelState.AddModelError(string.Empty, "Could not accept the request.");
                _logger.LogWarning(string.Empty, "Could not accept the authorization request from a coach");
            }
            return View(model);
        }

        [HttpPost]
        public IActionResult DenyRequest(AuthorizedID model)
        {
            if (!string.IsNullOrEmpty(model.IdCoach) && ModelState.IsValid)
            {
                AuthorizedID AcceptedRequest = _repo.GetSpecificAuthRequest(model.IdCoach, UserId());
                AcceptedRequest.StatusRequest = Status.Failed;
                AuthorizedID deniedAuthRequest = _repo.ModifyStatusAuthRequest(AcceptedRequest);
                if (deniedAuthRequest != null)
                {
                    _repo.UpdateAuthHistoryUpdate(deniedAuthRequest.ID, UserId());
                    return RedirectToAction("Authorizations", "Person");
                }
                ModelState.AddModelError(string.Empty, "Could not accept the request.");
                _logger.LogWarning(string.Empty, "Could not deny the authorization request from a coach");
            }
            return View(model);
        }
        #endregion
    }
}