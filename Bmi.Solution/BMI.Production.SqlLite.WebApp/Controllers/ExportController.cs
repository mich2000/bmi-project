﻿using BMI.Production.DAL.Classes;
using BMI.Production.DAL.Interfaces;
using BMI.Production.DAL.Models;
using BMI.Production.SqlLite.WebApp.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace BMI.Production.SqlLite.WebApp.Controllers
{
    [Authorize(Roles = Roles.Coach + "," + Roles.Admin + "," + Roles.Person)]
    [Authorize(Policy = "MustBeSignedIn")]
    public class ExportController : Controller, IIdentifierController
    {
        #region fields
        private readonly IBmiRepo _repo;
        #endregion

        #region User Info
        [Authorize(Policy = "MustBeSignedIn")]
        public string UserId() => User.FindFirstValue(ClaimTypes.NameIdentifier);

        [Authorize(Policy = "MustBeSignedIn")]
        public string UserEmail() => User.FindFirstValue(ClaimTypes.Email);

        [Authorize(Policy = "MustBeSignedIn")]
        public string UserRole() => User.FindFirstValue(ClaimTypes.Role);
        #endregion

        #region constructors
        public ExportController(IBmiRepo repo)
        {
            _repo = repo;
        }
        #endregion

        #region methods
        /**
         * For the 2 export functions I implement a check on the role of the coach to prevent abuse, because for user they get 
         * their data with their user id and with the admin he is allowed almost everything. But the coach is controlled if 
         * he contains the user id he has send through.
         * **/
        [HttpGet]
        public FileResult ReturnCsv(string userId)
        {
            if(UserRole() == Roles.Coach)
            {
                AuthorizedID auth = _repo.GetSpecificAuthRequest(UserId(), userId);
                if (auth == null)
                {
                    return null;
                }
            }
            SetupUserId(ref userId);
            IEnumerable<BmiLog> bmiLogs = _repo.GetBmiLogsByUser(userId) ?? throw new ArgumentNullException();
            return File(bmiLogs.ToList().ReturnCsvList().ByteEncoder(), "text/csv", "logs.csv");
        }

        [HttpGet]
        public FileResult ReturnMd(string userId)
        {
            if (UserRole() == Roles.Coach)
            {
                AuthorizedID auth = _repo.GetSpecificAuthRequest(UserId(), userId);
                if (auth == null)
                {
                    return null;
                }
            }
            SetupUserId(ref userId);
            IEnumerable<BmiLog> bmiLogs = _repo.GetBmiLogsByUser(userId) ?? throw new ArgumentNullException();
            return File(bmiLogs.ToList().ReturnMarkdownList().ByteEncoder(), "text/md", "logs.md");
        }
        #endregion

        #region Setup User Id
        public void SetupUserId(ref string userId)
        {
            if (User.IsInRole(Roles.Person) || userId == null)
                userId = UserId();
        }
        #endregion
    }
}