﻿using BMI.Production.DAL.Classes;
using BMI.Production.DAL.Context;
using BMI.Production.DAL.Interfaces;
using BMI.Production.DAL.Models;
using BMI.Production.WebApp.Classes;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BMI.Production.SqlLite.WebApp
{
    public static class StartupExtension
    {
        /**
         * This function is used to make migrations at the startup of our webserver, it also controls after the migrations if all
         * the necessary roles are present in the database and uses the role manager to insert them if they are missing. It also uses
         * the user manager to insert the admin into the database if he is not present.
         * **/
        public static async Task SeedUsersAndRoles(this IServiceProvider serviceProvider)
        {
            serviceProvider.GetRequiredService<BmiContext>().Database.EnsureCreated();
;           RoleManager<IdentityRole> RoleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            UserManager<BmiUser> UserManager = serviceProvider.GetRequiredService<UserManager<BmiUser>>();
            IBmiRepo repo = serviceProvider.GetRequiredService<IBmiRepo>();
            //controles of every role exists withtin the DB and adds them if needed.
            if (!RoleManager.RoleExistsAsync(Roles.Admin).Result)
                await RoleManager.CreateAsync(new IdentityRole(Roles.Admin));
            if (!RoleManager.RoleExistsAsync(Roles.Coach).Result)
                await RoleManager.CreateAsync(new IdentityRole(Roles.Coach));
            if (!RoleManager.RoleExistsAsync(Roles.Person).Result)
                await RoleManager.CreateAsync(new IdentityRole(Roles.Person));
            BmiUser admin = new BmiUser(UserMockInfo.AdminInfo.email);
            if (UserManager.InsertIfNotPresentAsync(admin, UserMockInfo.AdminInfo.pwd).Result.Succeeded)
            {
                if (!UserManager.IsInRoleAsync(admin, Roles.Admin).Result)
                {
                    await UserManager.AddToRoleAsync(admin, Roles.Admin);
                    BmiUser user = await UserManager.FindByEmailAsync(admin.Email);
                    repo.CreateUserHistoryInsert(user.Id, user.Id);
                }
            }
        }

        /**
         * function used to seed persons and coaches. It will seed 3 persons and 2 coaches the email and password are in the 
         * MockInfo static class. The coaches will also send a authorization request to the 3 persons that are mocked in the function.
         * All of these users are inserted only if they don't exist. The users in the person role will also get some logs as a way to 
         * seed them.
         * **/
        public static async Task SeedPersonsAndCoaches(this IServiceProvider serviceProvider)
        {
            IBmiRepo _repo = serviceProvider.GetRequiredService<IBmiRepo>();
            UserManager<BmiUser> UserManager = serviceProvider.GetRequiredService<UserManager<BmiUser>>();
            foreach ((string email, string pwd) in new (string email, string pwd)[]{ UserMockInfo.Person1,
                UserMockInfo.Person2 ,UserMockInfo.Person3 })
            {
                BmiUser bmiUser = new BmiUser(email);
                if (UserManager.InsertIfNotPresentAsync(bmiUser, pwd).Result.Succeeded)
                {
                    if (!UserManager.IsInRoleAsync(bmiUser, Roles.Person).Result)
                    {
                        await UserManager.AddToRoleAsync(bmiUser, Roles.Person);
                        BmiUser user = await UserManager.FindByEmailAsync(bmiUser.Email);
                        _repo.CreateUserHistoryInsert(user.Id, user.Id);
                        List<BmiLog> testLogs = new List<BmiLog>()
                        {
                            new BmiLog(new DateTime(2020,6,1), user.Id, 1.67M,67.5M),
                            new BmiLog(new DateTime(2020,6,3), user.Id, 1.67M,66.45M),
                            new BmiLog(new DateTime(2020,6,5), user.Id, 1.67M,65.2M),
                            new BmiLog(new DateTime(2020,6,7), user.Id, 1.67M,64.8M),
                        };
                        foreach (BmiLog log in testLogs)
                        {
                            BmiLog insertedLog = _repo.InsertLog(log);
                            _repo.CreateAuthHistoryInsert(insertedLog.ID, user.Id);
                        }
                    }
                }
            }
            foreach ((string email, string pwd) in new (string email, string pwd)[] { UserMockInfo.Coach1, UserMockInfo.Coach2 })
            {
                BmiUser bmiUser = new BmiUser(email);
                if (UserManager.InsertIfNotPresentAsync(bmiUser, pwd).Result.Succeeded)
                {
                    if (!UserManager.IsInRoleAsync(bmiUser, Roles.Coach).Result)
                    {
                        await UserManager.AddToRoleAsync(bmiUser, Roles.Coach);
                        BmiUser coach = await UserManager.FindByEmailAsync(bmiUser.Email);
                        _repo.CreateUserHistoryInsert(coach.Id, coach.Id);
                        IList<BmiUser> persons = UserManager.Users.ToList();
                        foreach (BmiUser user in persons)
                        {
                            if (user.Email == UserMockInfo.Person1.email || user.Email == UserMockInfo.Person2.email
                                || user.Email == UserMockInfo.Person3.email)
                            {
                                AuthorizedID auth = _repo.CreateAuth(coach.Id, user.Id);
                                _repo.UpdateAuthHistoryUpdate(auth.ID, coach.Id);
                            }
                        }
                    }
                }
            }
        }
    }
}