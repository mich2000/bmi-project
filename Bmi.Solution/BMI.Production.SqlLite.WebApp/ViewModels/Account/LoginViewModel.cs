﻿using System.ComponentModel.DataAnnotations;

namespace BMI.Production.SqlLite.WebApp.ViewModels.Account
{
    public class LoginViewModel
    {
        #region constructors
        public LoginViewModel(string email, string password, bool rememberMe)
        {
            Email = email;
            Password = password;
            RememberMe = rememberMe;
        }

        public LoginViewModel() { }
        #endregion

        #region properties
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "Remember me")]
        public bool RememberMe { get; set; }
        #endregion
    }
}