﻿using BMI.Production.DAL.Models;

namespace BMI.Production.SqlLite.WebApp.ViewModels.Coach
{
    public class DetailBmiLogOfUserViewModel
    {
        #region constructor
        public DetailBmiLogOfUserViewModel(string idUser, BmiLog bmiLog)
        {
            IdUser = idUser;
            BmiLog = bmiLog;
        }
        #endregion

        #region properties
        public string IdUser { get; set; }

        public BmiLog BmiLog { get; set; }
        #endregion
    }
}