﻿using System.ComponentModel.DataAnnotations;

namespace BMI.Production.SqlLite.WebApp.ViewModels.Coach
{
    public class AuthRequestViewModel
    {
        [Display(Name = "Email of the requested user: ")]
        public string EmailOfRequested { get; set; }
    }
}