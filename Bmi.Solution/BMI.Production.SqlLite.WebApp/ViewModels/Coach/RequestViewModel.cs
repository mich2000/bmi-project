﻿using BMI.Production.DAL.Models;
using System.Collections.Generic;
using System.Linq;

namespace BMI.Production.SqlLite.WebApp.ViewModels.Coach
{
    public class RequestViewModel
    {
        #region constructors
        public RequestViewModel(IEnumerable<AuthorizedID> acceptedRequests, IEnumerable<AuthorizedID> failedPendingRequest)
        {
            AcceptedRequests = acceptedRequests;
            FailedPendingRequest = failedPendingRequest;
        }
        #endregion

        #region properties
        public IEnumerable<AuthorizedID> AcceptedRequests { get; set; }
        public IEnumerable<AuthorizedID> FailedPendingRequest { get; set; }
        #endregion

        #region methods
        public int CountAllRequest()
        {
            return AcceptedRequests.Count() + FailedPendingRequest.Count();
        }
        #endregion
    }
}
