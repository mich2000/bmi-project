﻿using BMI.Production.DAL.Classes;
using System;
using System.ComponentModel.DataAnnotations;

namespace BMI.Production.SqlLite.WebApp.ViewModels.Admin
{
    public class LogCreationViewModel
    {
        #region fields
        private decimal _lenght;
        private decimal _weight;
        private DateTime _dateLog;
        #endregion

        #region properties
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Date measuring happened: ")]
        public DateTime DateLog
        {
            get { return _dateLog; }
            set
            {
                if (value != _dateLog) _dateLog = value;
            }
        }

        [Required]
        [Range(0, 9999999999999999.99, ErrorMessage = "Please enter valid decimalNumber, seperated by a dot.")]
        [Display(Name = "Length of the person: ")]
        public decimal Lenght
        {
            get { return _lenght; }
            set
            {
                if (value != _lenght) _lenght = value.To2Decimals();
            }
        }

        [Required]
        [Range(0, 9999999999999999.99, ErrorMessage = "Please enter valid decimalNumber, seperated by a dot.")]
        [Display(Name = "Weight of the person: ")]
        public decimal Weight
        {
            get { return _weight; }
            set
            {
                if (value != _weight) _weight = value.To2Decimals();
            }
        }
        #endregion
    }
}