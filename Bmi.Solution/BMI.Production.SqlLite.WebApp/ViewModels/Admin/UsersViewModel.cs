﻿using BMI.Production.DAL.Models;
using System.Collections.Generic;
using System.Linq;

namespace BMI.Production.SqlLite.WebApp.ViewModels.Admin
{
    public class UsersViewModel
    {
        #region properties
        public List<BmiUser> AllUsers { get; set; } = new List<BmiUser>();
        public List<BmiUser> AllCoaches { get; set; } = new List<BmiUser>();
        public List<BmiUser> AllUsersAndCoaches { get; set; } = new List<BmiUser>();
        #endregion

        #region Methods
        public int AllUsersCount() => AllUsers.Count() + AllCoaches.Count() + AllUsersAndCoaches.Count();
        #endregion
    }
}