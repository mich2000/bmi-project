﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace BMI.Production.SqlLite.WebApp.ViewModels.Admin
{
    public class UpdateUserViewModel
    {
        #region constructor
        public UpdateUserViewModel(string userId, string newUserName)
        {
            UserId = userId;
            NewUserName = newUserName;
        }

        public UpdateUserViewModel() : this("", "") { }
        #endregion

        #region properties
        [PersonalData]
        [Required]
        [Display(Name = "User name")]
        public string UserId { get; set; }


        [PersonalData]
        [Required]
        [Display(Name = "User Name")]
        public string NewUserName { get; set; }
        #endregion
    }
}