﻿using BMI.Production.DAL.Models;
using System.Collections.Generic;

namespace BMI.Production.SqlLite.WebApp.ViewModels.Admin
{
    public class UserViewModel
    {
        public UserViewModel(string userId, IEnumerable<BmiLog> bmiLogs)
        {
            UserId = userId;
            BmiLogs = bmiLogs;
        }

        public string UserId { get; set; }

        public IEnumerable<BmiLog> BmiLogs { get; set; }
    }
}