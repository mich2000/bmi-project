﻿using BMI.Production.DAL.Models;
using System.Collections.Generic;

namespace BMI.Production.SqlLite.WebApp.ViewModels.Admin
{
    public class AuthRequestsOfUserViewModel
    {
        public string UserId { get; set; }

        public IEnumerable<AuthorizedID> AuthorizedIDs { get; set; }
    }
}