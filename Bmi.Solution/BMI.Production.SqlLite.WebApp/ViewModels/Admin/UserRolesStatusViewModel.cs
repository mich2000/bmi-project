﻿using System.Collections.Generic;
using System.Linq;

namespace BMI.Production.SqlLite.WebApp.ViewModels.Admin
{
    public class UserRolesStatusViewModel
    {
        #region properties
        public string UserId { get; set; }

        public List<RoleOfUserViewModel> RolesStatus { get; set; } = new List<RoleOfUserViewModel>();
        #endregion

        #region methods
        public bool HasAtLeastOneRole()
        {
            if (RolesStatus.Count > 0)
            {
                return RolesStatus.Any(RolesStatus => RolesStatus.UserInRole);
            }
            return false;
        }
        #endregion
    }
}