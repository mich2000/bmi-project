﻿using BMI.Production.DAL.Classes;
using BMI.Production.SqlLite.WebApp.Enums;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BMI.Production.WebApp.Classes
{
    public static class UserManagerExtension
    {
        /**
         * function used to make sure the user exists if he doesn't exist yet.
         * **/
        public static async Task<IdentityResult> InsertIfNotPresentAsync<T, C>(this UserManager<T> _userManager, T newUser,
            string password, ILogger<C> _logger)
            where T : IdentityUser
            where C : Controller
        {
            T user = await _userManager.FindByEmailAsync(newUser.Email);
            if (user == null)
            {
                _logger.LogWarning(string.Empty, "A user has been added.");
                return await _userManager.CreateAsync(newUser, password);
            }
            _logger.LogWarning(string.Empty, "A user couldn't be added.");
            return IdentityResult.Failed(new IdentityError[] { new IdentityError() { Description = "There was no user" } });
        }

        public static async Task<IdentityResult> InsertIfNotPresentAsync<T>(this UserManager<T> _userManager, T newUser,
            string password)
            where T : IdentityUser
        {
            T user = await _userManager.FindByEmailAsync(newUser.Email);
            if (user == null)
            {
                return await _userManager.CreateAsync(newUser, password);
            }
            return IdentityResult.Failed(new IdentityError[] { new IdentityError() { Description = "There was no user" } });
        }

        /**
         * Function used that deletes all the roles of an user and then adds roles for him based on the ChosableRoles enum parameter.
         * **/
        public static async Task<IdentityResult> ImplementRolesForUserAsync<T, C>(this UserManager<T> _userManager, ILogger<C> _logger
            , T user, ChosableRoles chosenRoleOption)
            where T : IdentityUser
            where C : Controller
        {
            IdentityResult identityResult = null;
            IEnumerable<string> roles = await _userManager.GetRolesAsync(user);
            if (roles.Count() > 0)
            {
                await _userManager.RemoveFromRolesAsync(user, roles);
            }
            switch (chosenRoleOption)
            {
                case ChosableRoles.User:
                    if (!_userManager.IsInRoleAsync(user, Roles.Person).Result)
                    {
                        identityResult = await _userManager.AddToRoleAsync(user, Roles.Person);
                        _logger.LogInformation(string.Empty, "A user has been added to the person role");
                    }
                    if (_userManager.IsInRoleAsync(user, Roles.Coach).Result)
                    {
                        identityResult = await _userManager.RemoveFromRoleAsync(user, Roles.Coach);
                        _logger.LogInformation(string.Empty, "A user has been removed from the coach role.");
                    }
                    break;
                case ChosableRoles.Coach:
                    if (!_userManager.IsInRoleAsync(user, Roles.Coach).Result)
                    {
                        identityResult = await _userManager.AddToRoleAsync(user, Roles.Coach);
                        _logger.LogInformation(string.Empty, "A user has been added to the coach role");
                    }
                    if (_userManager.IsInRoleAsync(user, Roles.Person).Result)
                    {
                        identityResult = await _userManager.RemoveFromRoleAsync(user, Roles.Person);
                        _logger.LogInformation(string.Empty, "A user has been removed from the person role.");
                    }
                    break;
                case ChosableRoles.Both:
                    if (!_userManager.IsInRoleAsync(user, Roles.Coach).Result)
                    {
                        identityResult = await _userManager.AddToRoleAsync(user, Roles.Coach);
                        _logger.LogInformation(string.Empty, "A user has been added to the coach role");
                    }
                    if (!_userManager.IsInRoleAsync(user, Roles.Person).Result)
                    {
                        identityResult = await _userManager.AddToRoleAsync(user, Roles.Person);
                        _logger.LogInformation(string.Empty, "A user has been added to the coach role");
                    }
                    break;
            }
            return identityResult;
        }

        /**
         * function used to update a password of an user, through first deleting and then adding the user's password.
         * **/
        public static async Task<IdentityResult> UpdatePassword<T, C>(this UserManager<T> _userManager, ILogger<C> _logger
            , T user, string password)
            where T : IdentityUser
            where C : Controller
        {
            IdentityResult result = await _userManager.RemovePasswordAsync(user);
            if (result.Succeeded)
            {
                result = await _userManager.AddPasswordAsync(user, password);
                _logger.LogInformation(string.Empty, "Password has been removed");
                if (result.Succeeded)
                {
                    _logger.LogInformation(string.Empty, "Password has been added");
                    return IdentityResult.Success;
                }
            }
            return result ?? IdentityResult.Failed(new IdentityError[] {
                    new IdentityError() { Description = "Could not remove the password of the user." }
               });
        }
    }
}