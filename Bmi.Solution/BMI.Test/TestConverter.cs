using Bmi.BLL.Classes;
using NUnit.Framework;

namespace BMI.Test
{
    public class Tests
    {
        [Test]
        public void TestConvertDecimals()
        {
            float num1 = 1.255670f;
            double num2 = 10.5615F;
            int num3 = 10;
            int num4 = 0;

            Assert.AreEqual(1.25, ((decimal)num1).To2Decimals());
            Assert.AreEqual(10.56, ((decimal)num2).To2Decimals());
            Assert.AreEqual(10.00, ((decimal)num3).To2Decimals());
            Assert.AreEqual(0.0, ((decimal)num4).To2Decimals());
        }
    }
}