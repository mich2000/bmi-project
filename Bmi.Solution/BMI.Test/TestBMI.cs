﻿using Bmi.BLL.Interfaces;
using Bmi.BLL.Models;
using NUnit.Framework;

namespace BMI.Test
{
    class TestBMI
    {
        private BmiLog bmiSpongebob, bmiPatrick, bmiOtto;
        /**
         * Calculator: WolframeAlpha
         * Spongebob weighs 67.666 kg and measures 1.56 m.
         * bmi: 27.80 && bsa: 1.67 && ideal weight: 52.93
         * Patrick weighs 90 kg and measures 1.91 m.
         * bmi: 24.67 && bsa: 2.19 && ideal weight: 79.34
         * Otto weighs 50 kg and measures 1.755 m.
         * bmi: 16.32 && bsa: 1.60 && ideal weight: 66.60
         * **/
        [SetUp]
        public void SetUpBmi()
        {
            bmiSpongebob = new BmiLog(1.56M, 67.666M);
            bmiPatrick = new BmiLog(1.91M, 90M);
            bmiOtto = new BmiLog(1.755M, 50M);
        }

        [Test]
        public void TestSpongebob()
        {
            Assert.AreEqual(27.80M, bmiSpongebob.ReturnBMI());
            Assert.AreEqual(1.67M, bmiSpongebob.ReturnBSA());
            Assert.AreEqual(52.93M, bmiSpongebob.ReturnIdealWeight());
            Assert.AreEqual(BmiCategory.OverWeight, bmiSpongebob.ReturnBmiCategory());
        }

        [Test]
        public void TestPatrick()
        {
            Assert.AreEqual(24.67M, bmiPatrick.ReturnBMI());
            Assert.AreEqual(2.19M, bmiPatrick.ReturnBSA());
            Assert.AreEqual(79.34M, bmiPatrick.ReturnIdealWeight());
            Assert.AreEqual(BmiCategory.NormalWeight, bmiPatrick.ReturnBmiCategory());
        }

        [Test]
        public void TestOtto()
        {
            Assert.AreEqual(16.32M, bmiOtto.ReturnBMI());
            Assert.AreEqual(1.60M, bmiOtto.ReturnBSA());
            Assert.AreEqual(66.60M, bmiOtto.ReturnIdealWeight());
            Assert.AreEqual(BmiCategory.UnderWeight, bmiOtto.ReturnBmiCategory());
        }
    }
}
