﻿using Bmi.BLL.Classes;
using Bmi.BLL.Models;
using Bmi.Prototype.DAL.Interfaces;
using BMI.Prototype.DAL.Classes;
using BMI.Prototype.DAL.Factories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace BMI.Prototype.WebApp.Controllers
{
    public class BMIController : Controller
    {
        private readonly IRepo _repo;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public BMIController(IRepo repo, IHttpContextAccessor httpContextAccessor)
        {
            _repo = repo;
            _httpContextAccessor = httpContextAccessor;
        }

        public string ReturnPersonId() => _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;

        public IActionResult Index()
        {
            string userId = ReturnPersonId();
            Person person = _repo.GetUserWithLogs(userId);
            if (person == null)
                ExFac.ThrowNull();
            return View(person);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(BmiLog log)
        {
            if (log == null)
                ExFac.ThrowNull();
            if (ModelState.IsValid)
            {
                string userId = ReturnPersonId();
                _repo.InsertBMI(userId, log);
            }
            return RedirectToAction("Index", "BMI");
        }

        public IActionResult Delete(int id)
        {
            string userId = ReturnPersonId();
            BmiLog log = _repo.GetBmiLog(userId, id);
            if (log == null)
                ExFac.ThrowNull();
            if (log.ID == 0)
                ExFac.ThrowArgs();
            return View(log);
        }

        public IActionResult DeleteLog(int ID)
        {
            string userId = ReturnPersonId();
            BmiLog log = _repo.DeleteBMI(userId, ID);
            if (log == null)
                ExFac.ThrowNull();
            return RedirectToAction("Index", "BMI");
        }

        public IActionResult Details(int ID)
        {
            string userId = ReturnPersonId();
            BmiLog log = _repo.GetBmiLog(userId, ID);
            if (log == null)
                ExFac.ThrowNull();
            if (log.ID == 0)
                ExFac.ThrowArgs();
            return View(log);
        }

        public IActionResult Update(int id)
        {
            string userId = ReturnPersonId();
            BmiLog log = _repo.GetBmiLog(userId, id);
            if (log == null)
                ExFac.ThrowNull();
            if (log.ID == 0)
                ExFac.ThrowArgs();
            return View(log);
        }

        [HttpPost]
        public IActionResult Update(BmiLog log)
        {
            if (ModelState.IsValid)
            {
                string userId = ReturnPersonId();
                if (log == null)
                    ExFac.ThrowNull();
                if (log.ID == 0)
                    ExFac.ThrowArgs();
                BmiLog bmiLog = _repo.UpdateBMI(userId, log);
                if (bmiLog == null)
                    ExFac.ThrowNull();
                return RedirectToAction("details", "BMI", new { bmiLog.ID });
            }
            return View();
        }

        public FileResult ReturnCsv()
        {
            string userId = ReturnPersonId();
            Person person = _repo.GetUserWithLogs(userId);
            if (person == null)
                ExFac.ThrowNull();
            return File(person.BmiLogList.ReturnCsvList().ByteEncoder(), "text/csv", "logs.csv");
        }

        public FileResult ReturnMd()
        {
            string userId = ReturnPersonId();
            Person person = _repo.GetUserWithLogs(userId);
            if (person == null)
                ExFac.ThrowNull();
            return File(person.BmiLogList.ReturnMarkdownList().ByteEncoder(), "text/md", "logs.md");
        }
    }
}