﻿using Bmi.BLL.Interfaces;
using Bmi.Prototype.DAL.Classes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;

namespace BMI.Prototype.WebApp.Controllers
{
    [AllowAnonymous]
    public class ErrorController : Controller
    {
        [Route("Error/{statusCode}")]
        public IActionResult HttpStatusCodeHandler(int statusCode)
        {
            IStatusCodeReExecuteFeature statusCodeResult = HttpContext.Features.Get<IStatusCodeReExecuteFeature>();
            IUrl url = new UrlHolder(statusCode, statusCodeResult.OriginalPath, statusCodeResult.OriginalQueryString);
            return View("NotFound", url);
        }

        [AllowAnonymous]
        [Route("Error")]
        public IActionResult ExceptionHandler()
        {
            IExceptionHandlerPathFeature exceptionHandlerPathFeature = HttpContext.Features.Get<IExceptionHandlerPathFeature>();
            WebFailure failure = new WebFailure(exceptionHandlerPathFeature.Error);
            return View("Error", failure);
        }
    }
}
