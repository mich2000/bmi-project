﻿using Bmi.BLL.Models;
using BMI.Prototype.DAL.Factories;
using BMI.Prototype.WebApp.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace BMI.Prototype.WebApp.Controllers
{
    [AllowAnonymous]
    public class AccountController : Controller
    {
        private readonly UserManager<Person> _userManager;
        private readonly SignInManager<Person> _signInManager;

        public AccountController(UserManager<Person> userManager, SignInManager<Person> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> RegisterAsync(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model == null)
                    ExFac.ThrowNull();
                Person user = new Person(model.Email, model.Password);
                if (user == null)
                    ExFac.ThrowNull();
                var identityResult = await _userManager.CreateAsync(user, model.Password).ConfigureAwait(false);
                if (identityResult.Succeeded)
                {
                    await _signInManager.SignInAsync(user, false).ConfigureAwait(false);
                    return RedirectToAction("Index", "BMI");
                }
                foreach (var error in identityResult.Errors)
                    ModelState.AddModelError(string.Empty, error.Description);
            }
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> LogoutAsync()
        {
            await _signInManager.SignOutAsync().ConfigureAwait(false);
            return RedirectToAction("Login", "Account");
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> LoginAsync(LoginViewModel model, Uri returnUrl)
        {
            if (model is null)
                throw new ArgumentNullException(nameof(model));
            if (ModelState.IsValid)
            {
                var signInResult = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, false).ConfigureAwait(false);
                if (signInResult.Succeeded)
                {
                    if (!string.IsNullOrWhiteSpace(returnUrl.AbsoluteUri) || Url.IsLocalUrl(returnUrl.AbsoluteUri))
                    {
                        return LocalRedirect(returnUrl.AbsoluteUri);
                    }
                    return RedirectToAction("Index", "BMI");
                }
                ModelState.AddModelError(string.Empty, "Invalid Login attempt");
            }
            return View(model);
        }
    }
}