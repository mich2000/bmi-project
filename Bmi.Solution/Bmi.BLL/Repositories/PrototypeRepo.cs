﻿using Bmi.BLL.Models;
using Bmi.Prototype.DAL.Context;
using Bmi.Prototype.DAL.Interfaces;
using BMI.Prototype.DAL.Factories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace Bmi.Prototype.DAL.Repositories
{
    public class PrototypeRepo : IRepo
    {
        private readonly PrototypeContext _context;

        public PrototypeRepo(PrototypeContext context)
        {
            _context = context;
        }

        public Person GetById(string user_id)
        {
            return _context.Users.Find(user_id) ?? throw new ArgumentNullException();
        }

        public Person GetUserWithLogs(string user_id)
        {
            return _context.Users.AsNoTracking().Where(ps => ps.Id == user_id).Include(ps => ps.BmiLogList).SingleOrDefault()
                ?? throw new ArgumentNullException();
        }

        public BmiLog InsertBMI(string user_id, BmiLog bmiLog)
        {
            if (bmiLog == null)
                ExFac.ThrowNull();
            Person person = GetUserWithLogs(user_id);
            if (person == null)
                ExFac.ThrowNull();
            person.BmiLogList.Add(bmiLog);
            _context.Entry(bmiLog).State = EntityState.Added;
            int affectedRows = _context.SaveChanges();
            if (affectedRows > 0)
            {
                return _context.Entry(bmiLog).Entity;
            }
            return null;
        }

        public BmiLog UpdateBMI(string user_id, BmiLog bmiLog)
        {
            if (bmiLog == null)
                ExFac.ThrowNull();
            BmiLog bmiLogUpdated = GetUserWithLogs(user_id).BmiLogList.Where(log => log.ID == bmiLog.ID).SingleOrDefault();
            bmiLogUpdated.Lenght = bmiLog.Lenght;
            bmiLogUpdated.Weight = bmiLog.Weight;
            bmiLogUpdated.DateLog = bmiLog.DateLog;
            _context.Entry(bmiLogUpdated).State = EntityState.Modified;
            int affectedRows = _context.SaveChanges();
            if (affectedRows > 0)
            {
                return _context.Entry(bmiLog).Entity;
            }
            return null;
        }

        public BmiLog DeleteBMI(string user_id, BmiLog bmiLog)
        {
            if (bmiLog != null)
                ExFac.ThrowNull();
            Person ps = GetUserWithLogs(user_id);
            if (ps == null)
                ExFac.ThrowArgs();
            ps.BmiLogList.Remove(bmiLog);
            _context.Entry(bmiLog).State = EntityState.Deleted;
            int affectedRows = _context.SaveChanges();
            return (affectedRows > 0) ? _context.Entry(bmiLog).Entity : null;
        }

        public BmiLog DeleteBMI(string user_id, int bmiLogId)
        {
            Person ps = GetUserWithLogs(user_id);
            if (ps == null)
                ExFac.ThrowNull();
            BmiLog bmiLog = GetBmiLog(user_id, bmiLogId);
            ps.BmiLogList.Remove(bmiLog);
            _context.Entry(bmiLog).State = EntityState.Deleted;
            int affectedRows = _context.SaveChanges();
            return (affectedRows > 0) ? _context.Entry(bmiLog).Entity : null;
        }

        public BmiLog GetBmiLog(string user_id, int id_bmi_log)
        {
            return GetUserWithLogs(user_id).BmiLogList.FirstOrDefault(log => log.ID == id_bmi_log);
        }
    }
}