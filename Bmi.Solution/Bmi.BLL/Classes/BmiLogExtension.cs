﻿using Bmi.BLL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BMI.Prototype.DAL.Classes
{
    public static class BmiLogExtension
    {
        public static List<decimal> ReturnBmiList(this List<BmiLog> BmiLogList)
        {
            if (BmiLogList.Count != 0)
            {
                List<decimal> bmiList = new List<decimal>();
                BmiLogList.ForEach(log => bmiList.Add(log.ReturnBMI()));
                return bmiList;
            }
            return new List<decimal>();
        }

        public static List<decimal> ReturnWeightList(this List<BmiLog> BmiLogList)
        {
            if (BmiLogList.Count != 0)
            {
                List<decimal> weightList = new List<decimal>();
                BmiLogList.ForEach(log => weightList.Add(log.Weight));
                return weightList;
            }
            return new List<decimal>();
        }

        public static String ReturnMarkdownList(this List<BmiLog> BmiLogList)
        {
            if (BmiLogList.Count != 0)
            {
                StringBuilder builder = new StringBuilder();
                builder.Append("Date log | Weight | Lenght\n");
                builder.Append("--- | --- | ---\n");
                BmiLogList.ForEach(log =>
                {
                    builder.Append($"{log.DateLog.ToShortDateString()} | {log.Weight} | {log.Lenght}\n");
                });
                return builder.ToString();
            }
            return "* There are no logs.";
        }

        public static String ReturnCsvList(this List<BmiLog> BmiLogList)
        {
            if (BmiLogList.Count != 0)
            {
                StringBuilder builder = new StringBuilder();
                builder.Append("Date log;Weight;Lenght\n");
                BmiLogList.ForEach(log =>
                {
                    builder.Append($"{log.DateLog.ToShortDateString()};{log.Weight};{log.Lenght}\n");
                });
                return builder.ToString();
            }
            return "There are no logs.";
        }
    }
}