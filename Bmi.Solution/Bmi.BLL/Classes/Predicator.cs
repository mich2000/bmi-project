﻿using System;
using System.Collections.Generic;

namespace Bmi.Prototype.DAL.Classes
{
    class Predicator
    {
        #region constructor
        public Predicator()
        {
            PredList = new List<Func<object, bool>>();
        }
        #endregion

        #region properties
        public List<Func<object, bool>> PredList { get; set; }
        #endregion

        #region methods
        public void Add(bool condition, Func<Object, bool> predicate)
        {
            if (condition) PredList.Add(predicate);
        }

        public void Add(Func<bool> condition, Func<Object, bool> predicate)
        {
            if (condition.Invoke()) PredList.Add(predicate);
        }

        public bool Filter(object obj)
        {
            foreach (Func<object, bool> predicate in PredList)
                if (!predicate.Invoke(obj)) return false;
            return true;
        }
        public bool IsEmpty() => PredList.Count != 0;
        #endregion
    }
}
