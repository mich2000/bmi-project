﻿using Bmi.BLL.Interfaces;
using System.Net;

namespace Bmi.Prototype.DAL.Classes
{
    public class UrlHolder : IUrl
    {
        public UrlHolder(int statusCode, string path, string parameters)
        {
            StatusCode = statusCode;
            Path = path;
            Parameters = parameters;
        }

        public int StatusCode { get; set; }

        public string Path { get; set; }

        public string Parameters { get; set; }

        public string ErrorLog()
        {
            return $"{StatusCode} error occured. Path = {Path} and Querystring = {Parameters}";
        }

        public string NormalLog()
        {
            return $"Status {StatusCode}. Path = {Path} and Querystring = {Parameters}";
        }

        public string StatusCodeMessage()
        {
            return StatusCode switch
            {
                (int)HttpStatusCode.NotFound => "Sorry, the ressource you requested could not be found.",
                _ => "Status code has not be found or implemented yet",
            };
        }
    }
}