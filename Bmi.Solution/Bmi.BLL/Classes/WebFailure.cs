﻿using System;

namespace Bmi.Prototype.DAL.Classes
{
    public class WebFailure
    {
        public WebFailure(string errorMessage, string errorStackTrace)
        {
            ErrorMessage = errorMessage;
            ErrorStackTrace = errorStackTrace;
        }

        public WebFailure(Exception exception) : this(exception.Message, exception.StackTrace) { }

        public String ErrorMessage { get; set; }

        public String ErrorStackTrace { get; set; }

        public string LogError()
        {
            return $"The path {ErrorStackTrace} threw an exception {ErrorMessage}";
        }
    }
}
