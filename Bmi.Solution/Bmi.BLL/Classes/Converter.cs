﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;

namespace Bmi.BLL.Classes
{
    public static class Converter
    {
        public static decimal To2Decimals(this decimal number)
        {
            return Math.Floor(number * 100) / 100;
        }

        public static byte[] ByteEncoder(this string content)
        {
            return Encoding.UTF8.GetBytes(content);
        }

        public static string ListToJson(this List<decimal> decimalList)
        {
            return JsonSerializer.Serialize(decimalList);
        }
    }
}