﻿using System;

namespace BMI.Prototype.DAL.Factories
{
    public static class ExFac
    {
        public static ArgumentNullException ThrowNull()
        {
            throw new ArgumentNullException();
        }

        public static ArgumentException ThrowArgs()
        {
            throw new ArgumentException();
        }

        public static ArgumentNullException Null()
        {
            throw new ArgumentNullException();
        }

        public static ArgumentException Args()
        {
            throw new ArgumentException();
        }
    }
}
