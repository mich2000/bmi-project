﻿using Bmi.BLL.Classes;
using Bmi.BLL.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;

namespace Bmi.BLL.Models
{
    public class BmiLog : IBmi, IPersonLog
    {
        #region fields
        const double idealBmiConst = 21.75;
        private decimal _lenght;
        private decimal _weight;
        private DateTime _dateLog;
        #endregion

        #region properties
        [Key]
        public int ID { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Date measuring happened: ")]
        public DateTime DateLog
        {
            get { return _dateLog; }
            set
            {
                if (value != _dateLog) _dateLog = value;
            }
        }

        [Required]
        [Range(0, 9999999999999999.99, ErrorMessage = "Please enter valid decimalNumber")]
        [Display(Name = "Length of the person: ")]
        public decimal Lenght
        {
            get { return _lenght; }
            set
            {
                if (value != _lenght) _lenght = value.To2Decimals();
            }
        }

        [Required]
        [Range(0, 9999999999999999.99, ErrorMessage = "Please enter valid decimalNumber")]
        [Display(Name = "Weight of the person: ")]
        public decimal Weight
        {
            get { return _weight; }
            set
            {
                if (value != _weight) _weight = value.To2Decimals();
            }
        }
        #endregion

        #region constructor
        public BmiLog(int iD, DateTime dateLog, decimal lenght, decimal weight)
        {
            ID = iD;
            DateLog = dateLog;
            Lenght = lenght;
            Weight = weight;
        }

        public BmiLog(DateTime dateLog, decimal lenght, decimal weight)
            : this(0, dateLog, lenght, weight) { }

        public BmiLog(decimal lenght, decimal weight)
            : this(0, DateTime.Now, lenght, weight) { }

        public BmiLog() : this(0.0M, 0.0M) { }
        #endregion

        #region methods
        public decimal ReturnBMI()
        {
            return (_weight / (_lenght * _lenght)).To2Decimals();
        }

        public decimal ReturnBSA()
        {
            return (0.007184M * (decimal)Math.Pow((double)Weight, 0.425) *
                (decimal)Math.Pow((double)Lenght * 100.0, 0.725)).To2Decimals();
        }

        public decimal ReturnIdealWeight()
        {
            return ((decimal)idealBmiConst * _lenght * _lenght).To2Decimals();
        }

        public BmiCategory ReturnBmiCategory()
        {
            decimal bmi = ReturnBMI();
            if (bmi < 18.5M) return BmiCategory.UnderWeight;
            else if (bmi >= 18.5M && bmi < 25) return BmiCategory.NormalWeight;
            else if (bmi >= 25 && bmi < 30) return BmiCategory.OverWeight;
            else return BmiCategory.Obesitas;
        }
        #endregion
    }
}