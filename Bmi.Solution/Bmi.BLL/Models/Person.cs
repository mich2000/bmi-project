﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace Bmi.BLL.Models
{
    public class Person : IdentityUser
    {
        public Person(String email, String password)
        {
            base.UserName = email;
            base.Email = email;
            base.PasswordHash = password;
        }

        public Person() { }

        public List<BmiLog> BmiLogList { get; set; } = new List<BmiLog>();
    }
}