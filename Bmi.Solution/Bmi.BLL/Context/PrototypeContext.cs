﻿using Bmi.BLL.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Bmi.Prototype.DAL.Context
{
    public class PrototypeContext : IdentityDbContext<Person>
    {
        public PrototypeContext(DbContextOptions<PrototypeContext> options) : base(options)
        {
            Database.EnsureDeleted();
            Database.EnsureCreated();
        }

        public DbSet<BmiLog> BmiLogs { get; set; }
    }
}
