﻿namespace Bmi.BLL.Interfaces
{
    public interface IAuthorize
    {
        public int ID_coach { get; set; }

        public int ID_person { get; set; }
    }
}