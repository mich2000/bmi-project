﻿using Bmi.BLL.Models;

namespace Bmi.Prototype.DAL.Interfaces
{
    public interface IRepo
    {
        Person GetById(string user_id);

        Person GetUserWithLogs(string user_id);

        BmiLog InsertBMI(string user_id, BmiLog bmiLog);

        BmiLog DeleteBMI(string user_id, BmiLog bmiLog);

        BmiLog DeleteBMI(string user_id, int bmiLogId);

        BmiLog UpdateBMI(string user_id, BmiLog bmiLog);

        BmiLog GetBmiLog(string user_id, int id_bmi_log);
    }
}
