﻿namespace Bmi.BLL.Interfaces
{
    public enum BmiCategory { UnderWeight, NormalWeight, OverWeight, Obesitas }
    public interface IBmi
    {
        public decimal Lenght { get; set; }

        public decimal Weight { get; set; }

        public decimal ReturnBMI();

        public decimal ReturnBSA();

        public decimal ReturnIdealWeight();

        public BmiCategory ReturnBmiCategory();
    }
}
