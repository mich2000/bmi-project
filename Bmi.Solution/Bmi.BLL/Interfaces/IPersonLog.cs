﻿using System;

namespace Bmi.BLL.Interfaces
{
    public interface IPersonLog
    {
        public int ID { get; set; }

        public DateTime DateLog { get; set; }
    }
}