﻿using System;

namespace Bmi.BLL.Interfaces
{
    public interface IUrl
    {
        int StatusCode { get; set; }
        String Path { get; set; }
        String Parameters { get; set; }
        String StatusCodeMessage();
        String NormalLog();
        String ErrorLog();
    }
}