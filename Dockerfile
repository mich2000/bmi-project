# Build stage
FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS builder

COPY . .

WORKDIR /Bmi.Solution

WORKDIR BMI.Production.SqlLite.WebApp

RUN dotnet publish -r linux-arm -c Release --self-contained true /p:PublishSingleFile=true /p:PublishTrimmed=true

# Final stage
FROM debian:stretch-slim

COPY --from=builder /Bmi.Solution/BMI.Production.SqlLite.WebApp/bin/Release/netcoreapp3.1/linux-arm/publish .

RUN chmod 777 ./BMI.Production.SqlLite.WebApp

CMD ["./BMI.Production.SqlLite.WebApp"]
